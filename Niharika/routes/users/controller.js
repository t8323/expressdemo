const { writeFile, readFileSync, writeFileSync } = require("fs");
const messages = require("../../messages");
//const users = require("D:/Study/training/Node/express_router_folder/users.json");
const user = require("./users.json");
//const validator = require("./validator");
const { v4: uuidv4 } = require("uuid");
//const e = require("express");
//const jwt = require("jsonwebtoken");
//const req = require("express/lib/request");
//const { response } = require("express");
//const { use } = require("express/lib/application");
//const { writeDataToFile } = require("./utils");
require("dotenv").config();
class User1 {
  signup(req, res) {
    const { firstname, lastname, emailId, password } = req.body;
    if (user.findIndex((ele) => ele.emailId === emailId) > -1)
      return res.status(406).json({ message: "emailId already exists" });
    Object.assign(req.body, { id: uuidv4() });
    user.push(req.body);
    writeFileSync(
      require("path").join(__dirname, "users.json"),
      JSON.stringify(user)
    );
    return res
      .status(messages.status.statusSuccess)
      .json(messages.messages.registeredSuccess);
  }

  signin(req, res) {
    console.log("user");
    const { sEmail, sPassword } = req.body;
    if (
      user.findIndex((ele) => ele.emailId === sEmail) > -1 &&
      user[user.findIndex((ele) => ele.emailId === sEmail)].password ==
        sPassword
    ) {
      user[user.findIndex((ele) => ele.emailId === sEmail)].isLoggedIn = true;
      user[user.findIndex((ele) => ele.emailId === sEmail)].orderArray = [];
      writeFileSync(
        require("path").join(__dirname, "users.json"),
        JSON.stringify(user)
      );
      return res
        .status(messages.status.statusSuccess)
        .json(messages.messages.loginSuccess);
    }
    if (
      user.findIndex((ele) => ele.emailId === sEmail) > -1 &&
      user[user.findIndex((ele) => ele.emailId === sEmail)].password !=
        sPassword
    ) {
      return res
        .status(messages.status.badrequest)
        .json(messages.messages.wrongpass);
    } else {
      return res
        .status(messages.status.statusNotFound)
        .json(messages.messages.loginEmailNotRegistered);
    }
  }

  // }

  delete(req, res) {
    const { sEmail, sPassword } = req.body;
    console.log(user.findIndex((ele) => ele.emailId === sEmail));
    if (
      user.findIndex((ele) => ele.emailId === sEmail) > -1 &&
      user[user.findIndex((ele) => ele.emailId === sEmail)].password ===
        sPassword
    ) {
      user.splice(
        user.findIndex((ele) => ele.emailId === sEmail),
        1
      );
      writeFileSync(
        require("path").join(__dirname, "users.json"),
        JSON.stringify(user)
      );

      return res.json({ message: "Deleted" });
    } else {
      res.json({ message: "not found" });
    }
  }

  //Password
  ediProfile(req, res) {
    const { firstname, lastname, email } = req.body;
    const found = user.some((ele) => ele.emailId === email);
    if (found) {
      user.forEach((element) => {
        if (element.emailId === email) {
          element.firstname = firstname ? firstname : element.firstname;

          element.lastname = lastname ? lastname : element.lastname;
        }
      });

      writeFileSync(
        require("path").join(__dirname, "users.json"),
        JSON.stringify(user)
      );

      return res
        .status(messages.status.statusSuccess)
        .json(messages.messages.updatedProfile);
    } else {
      return res
        .status(messages.status.statusNotFound)
        .json(messages.messages.loginEmailNotRegistered);
    }
  }
  changePass(req, res) {
    const { email, oldPassword, newPassword } = req.body;
    const foundPass = user.some(
      (ele) => ele.emailId === email && ele.password === oldPassword
    );
    const notFoundPass = user.some(
      (ele) => ele.emailId === email && ele.password !== oldPassword
    );
    if (foundPass) {
      user.forEach((ele) => {
        if (ele.password === oldPassword)
          ele.password = newPassword ? newPassword : ele.password;
      });
      writeFileSync(
        require("path").join(__dirname, "users.json"),
        JSON.stringify(user)
      );
      return res
        .status(messages.status.statusSuccess)
        .json(messages.messages.changePass);
    }
    if (notFoundPass) {
      return res
        .status(messages.status.statusNotFound)
        .json(messages.messages.wrongpass);
    } else {
      return res
        .status(messages.status.statusNotFound)
        .json(messages.messages.loginEmailNotRegistered);
    }
  }
}

module.exports = new User1();
