const messaege = require("../../messages");
let user = require("./users.json");
class validators {
  signUp = (req, res, next) => {
    const { firstname, lastname, emailId, password } = req.body;
    if (!emailId) return res.status(403).json({ message: "Email is requierd" });
    if (!password)
      return res.status(403).json({ message: "Password is requierd" });
    if (!firstname)
      return res.status(403).json({ message: "Firstname is requierd" });
    if (!lastname)
      return res.status(403).json({ message: "Lastname is requierd" });
    next();
  };

  login = (req, res, next) => {
    const { sEmail, sPassword } = req.body;
    if (!sEmail)
      return res
        .status(messaege.status.statusNotFound)
        .json(messaege.messages.emailReq);
    if (!sPassword)
      return res
        .status(messaege.status.statusNotFound)
        .json(messaege.messages.passReq);
    next();
  };

  editProfile = (req, res, next) => {
    const { sEmail, sPassword } = req.body;
    if (
      user.findIndex((ele) => ele.emailId === sEmail) > -1 &&
      user[user.findIndex((ele) => ele.emailId === sEmail)].password ===
        sPassword
    )
      return res
        .status(messaege.status.statusSuccess)
        .json(messaege.messages.process);

    if (
      user.findIndex((ele) => ele.emailId !== sEmail) > -1 &&
      user[user.findIndex((ele) => ele.emailId !== sEmail)].password !==
        sPassword
    )
      return res
        .status(messaege.status.statusNotFound)
        .json(messaege.messages.invalidCredentials);

    next();
  };
}

module.exports = new validators();
