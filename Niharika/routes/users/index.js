const indexRouter = require("express").Router();
const controller = require("./controller");
const validator = require("./validator");

indexRouter.post("/signup", validator.signUp, controller.signup);
indexRouter.post("/signin", validator.login, controller.signin);
indexRouter.delete("/delete", controller.delete);
indexRouter.post("/updateProfile", controller.ediProfile);
indexRouter.post("/updatePassword", controller.changePass);
module.exports = indexRouter;
