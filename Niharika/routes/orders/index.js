const indexOrderRouter = require("express").Router();
const controller1 = require("./controllerOrder");
indexOrderRouter.get("/getOrder", controller1.getOrders);
indexOrderRouter.post("/placeOrder", controller1.placeOder);
module.exports = indexOrderRouter;
