const routes = require("express").Router();
const req = require("express/lib/request");
const user = require("./users");
const order = require("./orders");
const jwt = require("jsonwebtoken");
const express = require("express"); //const controller = require("./users/controller");
routes.use("/user", user);

routes.use("/order", order);
routes.use(express.json());
/*
// routes.get("/signup", (req, res) => {
//   //res.json(validator.signup);
//   res.send("Hello Niharika");
// });*/
routes.all("*", (req, res) => {
  res.send("Route not found");
});

module.exports = routes;
