const status = {
  statusSuccess: 200,
  statusNotFound: 403,
  badrequest: 400,
};

module.exports = status;
