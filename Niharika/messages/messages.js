const messages = {
  registeredSuccess: { messaege: "You are registered successfully" },
  loginSuccess: { messaege: "You are logged in successfully" },
  loginEmailNotRegistered: { messaege: "Email not registered" },
  wrongpass: { messaege: "Password doesnt match" },
  emailReq: { messaege: "Email id is required" },
  passReq: { messaege: "Password is required" },
  fistNameReq: { messaege: "Firstname required" },
  lastNameReq: { messaege: "Lastname required" },
  updatedProfile: { messaege: "Profile successfully updated" },
  process: { messaege: "you can edit now" },
  invalidCredentials: { messaege: "Invalid credentials" },
  doNotChangeEmail: { messaege: "Cannot change email" },
  changePass: { messaege: "Password changed successfully" },
  orderSucess: { messaege: "Order placed successfully" },
};

module.exports = messages;
