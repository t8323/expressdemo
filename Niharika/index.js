const express = require("express");
const body_parser = require("body-parser");
const routes = require("./routes/route");
const app = express();
app.use(body_parser.json());
app.use(body_parser.urlencoded({ extended: true }));
require("dotenv").config();
console.log(process.env.PORT);
console.log(process.env.ACCESS);
app.use("/", routes);
app.listen(process.env.PORT, () => {
  console.log("Listening" + process.env.PORT);
});
