require('dotenv').config();
let express = require('express');
let app = express();

let sellerRouter = require('./Routes/sellers');
let userRouter = require('./Routes/Auth');
let adminRouter = require("./routes/admin");
let BuyerRoute = require('./routes/Buyers');

let cookieParser = require('cookie-parser');
let helmet = require('helmet');
let cors = require('cors');
let port = process.env.POT;
app.use(helmet());
app.use(express.json());
app.use(cookieParser());
app.use(cors());

app.use("/api/user",userRouter);
app.use("/api/products",sellerRouter);
app.use("/api/admin",adminRouter);
app.use("/api/buyer",BuyerRoute);

app.all("*",(req,res)=>{
    res.sendStatus =404;
    res.send("Page Not Found")
    
})


 
app.listen(port,()=>{
    console.log(`Listen successfully on Port ${port}`);
})