
// validation for unique email
let fs = require('fs');
let validator = require('validator');
const { readFile } = require('../../fileReader');
let statusandmsg = require('../../status');
let pathUser = "./users.json";
function registerValidator(req, res, next) {
    try {
        let { firstName: bfirstName, lastName: blastName, password: bpassword, email: bemail } = req.body;
        if(!(typeof bfirstName == "string" && typeof blastName == "string" && typeof bemail == "string" && typeof bpassword == "string")){
          return  res.status(statusandmsg.badrequest).send({message:statusandmsg.allfieldmandmsg});
        }
        if (!(bfirstName && blastName && bpassword && bemail && bfirstName.trim().length > 0 && blastName.trim().length > 0 && bpassword.trim().length > 0 && bemail.trim().length > 0)) {
        return  res.status(statusandmsg.badrequest).send({ message: statusandmsg.allfieldmandmsg });
        }

        let users = readFile(pathUser);
        console.log(users);
        let checkEmail = users.find(ele=>ele.email == bemail  );
        if(checkEmail) return res.status(statusandmsg.badrequest).send({message:statusandmsg.uniqueEmail});
        next();
         

    } catch (error) {
        res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }


}

function profileUpdateValidator(req, res, next) {
    try {
        let { firstName: bfirstName, lastName: blastName, email: bemail } = req.body;
        if(!(typeof bfirstName == "string" && typeof blastName == "string" && typeof bemail == "string" && bfirstName.trim().length >0 &&blastName.trim().length >0 && bemail.trim().length >0)){
            return res.status(statusandmsg.badrequest).send({message: statusandmsg.allfieldmandmsg});
        }
         
        else {
            let arr = readFile(pathUser);
            let checkUniqueEmail = arr.filter((ele) => {
                return ele.email == bemail && ele.email != req.user.email;
            });
            if (validator.isEmail(bemail)) {
                if (checkUniqueEmail.length > 0) {
                    res.status(statusandmsg.badrequest).send({ message: statusandmsg.uniqueEmail });
                } else {
                    next();
                }
            } else {
                res.status(statusandmsg.badrequest).send({ message: statusandmsg.invalidemailmsg });
            }
        }

    } catch (error) {
        res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }


}
function loginValidator(req, res, next) {
    let { email: bemail, password: bpassword } = req.body;
    if(!(typeof bemail == "string" && typeof bpassword == "string" && bemail.trim().length>0 && bpassword.trim().length >0)){
        return res.status(statusandmsg.badrequest).send({message:statusandmsg.allfieldmandmsg});
    }
    else {
        next();
    }
}

function changePasswordvalidator(req, res, next) {
    let { oldPassword: boldPassword, newPassword: bnewPassword, confirmPassword: cpass } = req.body;
    if(!(typeof boldPassword == "string" && typeof bnewPassword == "string" && typeof cpass == "string" && bnewPassword.trim().length>0 && boldPassword.trim().length >0 && cpass.trim().length>0)){
        return res.status(statusandmsg.badrequest).send({message:statusandmsg.allfieldmandmsg});
    }else {
        next();
    }
}



module.exports = { registerValidator, profileUpdateValidator, loginValidator, changePasswordvalidator };