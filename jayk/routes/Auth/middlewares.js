let { readFile ,writeFile} = require('../../fileReader');
let statusandmsg = require('./../../status');
let jwt = require('jsonwebtoken');
 
let pathUser = "./users.json";


function isAuth(req, res, next) {

    let token = req.header('authorization') || req.body.token || "";

    let userData = readFile(pathUser);
    try {

        if (!token) return res.status(statusandmsg.unauthorizedcode).send({ message: statusandmsg.notloginerrmsg });
        token = token.substring(7);
        let user = jwt.verify(token, process.env.SECRETE_KEY);
        let authuser = userData.find((ele) => {
            return user.id == ele.id;
        })
        if (!(authuser && authuser.tokenarr.includes(token))) return res.status(statusandmsg.unauthorizedcode).send({ message: statusandmsg.unauthorizedmsg });
        req.user = { ...authuser, token };
        next();
    } catch (error) {
        
        if (error.message == "jwt expired") {
            const payload = jwt.verify(token, process.env.SECRETE_KEY, { ignoreExpiration: true });
             let userIndex = userData.findIndex(ele=>ele.id == payload.id);
            let tokenIndex=userData[userIndex].tokenarr.findIndex(ele=>ele==token);
            if(tokenIndex == -1) return res.status(statusandmsg.unauthorizedcode).send({message:statusandmsg.invalidtokenmsg});
            userData[userIndex].tokenarr.splice(tokenIndex,1);
            writeFile(pathUser,userData);
            return res.status(statusandmsg.successcode).send({message:statusandmsg.sessionexpiredmsg});
        } else{

          return  res.status(statusandmsg.unauthorizedcode).send({ message: error.message });
        }
    }

}

function isAdmin(req, res, next) {
    try {
        if (req.user.role == 2) next();
        else return res.status(statusandmsg.unauthorizedcode).send({ message: statusandmsg.isadminerrmsg });
    } catch (error) {
        return res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }
}

function isBuyer(req, res, next) {
    try {
        if (req.user.role == 0) next();
        else return res.status(statusandmsg.unauthorizedcode).send({ message: statusandmsg.buyererrmsg })
    } catch (error) {
        return res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }
}

function isSeller(req, res, next) {
    try {
        if (req.user.role == 1) next();
        else return res.status(statusandmsg.unauthorizedcode).send({ message: statusandmsg.sellererrmsg });
    } catch (error) {
        return res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }
}



module.exports = { isAuth, isAdmin, isBuyer, isSeller };