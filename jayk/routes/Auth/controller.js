
const jwt = require('jsonwebtoken');
let { writeFile, readFile } = require('../../fileReader');
let statusandmsg = require('../../status');
let fs = require('fs');
var bcrypt = require('bcryptjs');

let pathUser = "./users.json";

const Register = (req, res) => {
    try {
        if (fs.existsSync(pathUser)) {
            let arr = readFile(pathUser);
            req.body.id = Math.floor(1000 + Math.random() * 9000);
            req.body.role = req.body.role ? req.body.role : 0;
            req.body.tokenarr = [];
            let salt = bcrypt.genSaltSync(10);
            let hash = bcrypt.hashSync(req.body.password, salt);
            req.body.password = hash;
                arr.push(req.body);
            writeFile(pathUser, arr);
            res.status(statusandmsg.successcode).send({ message: statusandmsg.registredmsg });

        } else {
            req.body.id = Math.floor(1000 + Math.random() * 9000);
            req.body.tokenarr = [];
            let salt = bcrypt.genSaltSync(10);
            let hash = bcrypt.hashSync(req.body.password, salt);
            req.body.password = hash;
            req.body.role = req.body.role ? req.body.role : 0;
            let arr = [req.body];
            writeFile(pathUser, arr);
            res.status(statusandmsg.successcode).send({ message: statusandmsg.registredmsg });
        }
    } catch (error) {
        res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }


}

const Login = (req, res) => {
    try {
        let arr = readFile(pathUser);
        let userIndex = arr.findIndex((ele) => {
            return ele.email == req.body.email ;
        })
   
        if(userIndex == -1) return res.status(statusandmsg.unauthorizedcode).send({message:statusandmsg.unauthorizedmsg});
        let comparePassword = bcrypt.compareSync(req.body.password, arr[userIndex].password);
        if(!comparePassword) return res.status(statusandmsg.unauthorizedcode).send({message:statusandmsg.invalidLogedinmsg}) 
        
            const token = jwt.sign(
                { id: arr[userIndex].id },
                process.env.SECRETE_KEY,
                { expiresIn: '10s' }
            );
            if (arr[userIndex].tokenarr.length === 3) arr[userIndex].tokenarr.shift();
            arr[userIndex].tokenarr.push(token);
            writeFile(pathUser, arr);
           return res.status(statusandmsg.successcode).send({ message: statusandmsg.logedInmsg, token });
         
    } catch (error) {
        res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }
}

const editProfile = (req, res) => {
    try {
        let data = readFile(pathUser);
        let ndata = data.map((ele) => {
            if (ele.id == req.user.id) {
                return { ...ele, firstName: req.body.firstName, lastName: req.body.lastName, email: req.body.email }
            }
            return ele;
        })
        writeFile(pathUser, ndata);

        res.status(statusandmsg.successcode).send({ message: statusandmsg.profileupdatesuccessmsg });
    } catch (error) {
        res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }

}

const changepassword = (req, res) => {
    try {
        let data = readFile(pathUser);
        let { oldPassword: olpass, newPassword: npass, confirmPassword: cpass } = req.body;
        let checkOpass = data.findIndex(ele => ele.password == olpass && ele.email == req.user.email);
        if (checkOpass == -1) res.status(statusandmsg.badrequest).send({ message: statusandmsg.wrongoldpassmsg })
        else if (npass != cpass) res.status(statusandmsg.badrequest).send({ message: statusandmsg.conforimpassnotmatmsg });
        else {
            data[checkOpass].password = npass;
            writeFile(pathUser, data);
            res.status(statusandmsg.successcode).send({ message: statusandmsg.passchangesucmsg });
        }
    } catch (error) {
        res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }






}

const logout = (req, res) => {
    let userdata = readFile(pathUser);
    let logedUserIndex = userdata.findIndex((ele) => {
        return ele.id == req.user.id;
    })
    let tokenIndex = userdata[logedUserIndex].tokenarr.findIndex((ele) => {
        return ele == req.user.token;
    })
    userdata[logedUserIndex].tokenarr.splice(tokenIndex, 1);
    writeFile(pathUser, userdata);
    return res.status(statusandmsg.successcode).send({ message: statusandmsg.logOutmsg });
}
module.exports = { Register, Login, editProfile, changepassword, logout };