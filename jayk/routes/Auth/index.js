
let router = require('express').Router();
let { registerValidator,profileUpdateValidator,loginValidator,changePasswordvalidator } = require('./validator');
let {isAuth} = require('./middlewares');
let { Register,Login,editProfile,changepassword,logout } = require("./controller");


router.post("/register", registerValidator, Register);
router.post("/login", loginValidator,Login);
router.post("/editprofile/:userid", isAuth,profileUpdateValidator, editProfile);
router.post("/changepassword/:userid",isAuth,changePasswordvalidator,changepassword);
router.get("/logout",isAuth,logout);




module.exports = router;
