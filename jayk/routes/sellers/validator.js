
let condeandmsg = require('../../status/index');

function addProductValidator(req,res,next){
   let {name:sname,price:sprice,quantity:squantity} = req.body;
   if(!(sname && sprice && squantity )) return res.status(condeandmsg.badrequest).send({message: condeandmsg.allfieldmandmsg});
   if(!(typeof sname == "string" && typeof sprice =="string" && typeof squantity == "string"))res.status(condeandmsg.badrequest).send({message:condeandmsg.typestringerrmsg})
   next();

}

module.exports = {addProductValidator};