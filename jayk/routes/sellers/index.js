let router = require('express').Router();
let {isAuth,isSeller} = require("../Auth/middlewares");
let {myProducts, addproduct} = require('./controller');
let {addProductValidator}=require('./validator');

router.get("/myproducts/",isAuth,isSeller,myProducts);
router.post("/addproduct/",isAuth,isSeller,addProductValidator,addproduct);
 
module.exports = router;