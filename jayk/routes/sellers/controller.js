let  {writeFile,readFile} = require('../../fileReader');
let condeandmsg = require('../../status/index');
let fs =require('fs');
let path = "./products.json";

function myProducts(req,res){
     let products = readFile(path);
     products = products.filter((ele)=>{
         return ele.uid == req.user.id;
     })
     return res.status(condeandmsg.successcode).send({message:condeandmsg.productFetchedmsg,products})
}

function addproduct(req,res){
  
    if(fs.existsSync(path)){
        let products = readFile(path);
        products.push({...req.body,id:Date.now(),uid:req.user.id});
        writeFile(path,products);
        res.status(condeandmsg.createsuccessCode).send({message: condeandmsg.productaddmsg});
    }else{
        console.log("fird");
        let Products = [{...req.body,id:Date.now(),uid:req.user.id}];
        writeFile(path,Products);
        res.status(condeandmsg.createsuccessCode).send({message: condeandmsg.productaddmsg});
    }
   
}
module.exports=  {myProducts,addproduct}