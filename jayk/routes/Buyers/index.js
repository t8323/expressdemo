let router = require('express').Router();
let {buyProducts,Products} = require("./controller");
let {manageQuantity} = require('./middleware');
let {isAuth,isBuyer} = require('../Auth/middlewares');

router.post("/buyproduct",isAuth,isBuyer,manageQuantity,buyProducts);
router.get("/products",isAuth,isBuyer,Products);

module.exports = router;