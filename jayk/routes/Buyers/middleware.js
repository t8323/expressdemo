
let { writeFile, readFile } = require("../../fileReader");
let fs = require('fs');
let condeandmsg = require('../../status/index');
function manageQuantity(req, res, next) {
    try {
        let { id: bid, quantity: bquantity } = req.body;
        let data = readFile("./products.json");
        let indexOfprod = data.findIndex((ele) => ele.id == bid)
        if (!(bid && bquantity > 0 && bid.toString().trim().length > 0)) res.status(condeandmsg.badrequest).send({ message: condeandmsg.validqtymsg });
        if(!(typeof bid == "string" && typeof bquantity =="string"))res.status(condeandmsg.badrequest).send({message:condeandmsg.typestringerrmsg});
        else if (indexOfprod == -1) res.status(condeandmsg.badrequest).send({ message: condeandmsg.prodnotfoundmsg })
        else if (data[indexOfprod].quantity <= 0) res.status(condeandmsg.badrequest).send({ message:  condeandmsg.prodoutofstockmsg});
        else if(data[indexOfprod].quantity - bquantity <0)res.status(condeandmsg.badrequest).send({message:condeandmsg.notsufqty})
        else {
           
         
            //write products
           next();
        }
    } catch (error) {
        console.log(error);
        res.status(condeandmsg.internalservererrorcode).send({ message: error.message });
    }
}

module.exports = { manageQuantity };