
const { writeFile, readFile } = require("../../fileReader");
const condeandmsg = require("../../status");
let fs = require('fs');
let pathOrder = "./orders.json";
let pathProducts = "./products.json";
function buyProducts(req, res) {

    let data = readFile(pathProducts);
    let indexOfprod = data.findIndex((ele) => {
        return ele.id == req.body.id;
    })
    data[indexOfprod].quantity = data[indexOfprod].quantity - req.body.quantity;
     writeFile(pathProducts,data);
    if (fs.existsSync(pathOrder)) {
        let orders = readFile(pathOrder);
        orders.push({ order_id: Date.now(), uid: req.user.id, product: data[indexOfprod], quantity: req.body.quantity });
        writeFile(pathOrder, orders);
    } else {
        let orders = [{ order_id: Date.now(), uid: req.user.id, product: data[indexOfprod], quantity: req.body.quantity }];
        writeFile(pathOrder, orders);
    }

    return res.status(condeandmsg.successcode).send({ message: condeandmsg.productsucessmsg });
}

function Products(req, res) {
    let products = readFile(pathProducts);
    res.status(condeandmsg.successcode).send({ message: condeandmsg.productFetchedmsg, products })
}

module.exports = { buyProducts, Products };