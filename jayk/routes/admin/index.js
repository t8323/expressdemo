let router = require('express').Router();
let {getusers,editUser,deleteUser,userOrders,updateOrders} = require('./controller');
let {isAuth} =require("../Auth/middlewares");
let {isAdmin}  = require("../Auth/middlewares");
let {profileUpdateValidator,updateOrdersValidator} = require("./validator");


router.get("/users",isAuth,isAdmin,getusers);
router.post("/editUser/:userid",isAuth,isAdmin,profileUpdateValidator,editUser);
router.post("/deleteUser/:userid",isAuth,isAdmin,profileUpdateValidator,deleteUser);
router.get("/userOrders",isAuth,isAdmin,userOrders);
router.get("/userOrders/update/:orderid",isAuth,isAdmin,updateOrdersValidator,updateOrders);

module.exports = router;
