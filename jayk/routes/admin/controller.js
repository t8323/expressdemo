const { readFile, writeFile } = require("../../fileReader");
let statusandmsg = require("../../status"); 
let fs = require('fs');
let pathUser = "./users.json";
function getusers(req,res){
    let users =  readFile(pathUser);
     users = users.filter((ele)=>{
         return ele.role  in [0,1];
     })
    return res.status(statusandmsg.successcode).send({message:statusandmsg.userfetchedsucmsg});
}

function editUser(req,res){
    try {
        let data = readFile(pathUser);
        let userIndex = data.findIndex((ele)=>{
            return ele.id == req.params.userid ;
        })
        if(userIndex == -1) return res.status(statusandmsg.notfoundcode).send({message:statusandmsg.usernotfoundmsg});
          data[userIndex] = {...data[userIndex],firstName:req.body.firstName,lastName:req.body.lastName,email:req.body.email};
         writeFile(pathUser,data);
      return  res.status(statusandmsg.successcode).send({ message: statusandmsg.profileupdatesuccessmsg });
    } catch (error) {
        res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }

}


function deleteUser(req,res){
    let data = readFile(pathUser);
    let userIndex = data.findIndex((ele)=>{
        return ele.id == req.params.userid ;
    })
    if(userIndex == -1) return res.status(statusandmsg.notfoundcode).send({message:statusandmsg.usernotfoundmsg});
    data.splice(userIndex,1);
    writeFile(pathUser,data);
    return res.status(statusandmsg.successcode).send({message:statusandmsg.userdeletedsucmsg});
}


function userOrders(req,res){
  if(fs.existsSync("./orders.json")){
      let orders =JSON.parse(fs.readFileSync('./orders.json',"utf-8"));
      return res.status(statusandmsg.successcode).send({message: statusandmsg.orderfetchsussessmsg,orders})
  }
}

function updateOrders(req,res){
    let orders =  JSON.parse(fs.readFileSync("./orders.json"));
    let orderIndex = orders.findIndex((ele)=>{
        return ele.order_id == req.params.orderid;
    })
    orders[orderIndex].quantity = req.body.quantity;
    console.log(orders[orderIndex]);
    fs.writeFileSync("./orders.json",JSON.stringify(orders));
    return res.status(statusandmsg.successcode).send({message:statusandmsg.qtyupdatesucmsg})
}

module.exports = {getusers,editUser,deleteUser,userOrders,updateOrders};