let statusandmsg = require("../../status");
const { readFile } = require("../../fileReader");
let validator =require('validator');
let fs = require('fs');
let pathUser = "./users.js";
function profileUpdateValidator(req, res, next) {
    try {
        let { firstName: bfirstName, lastName: blastName, email: bemail } = req.body;
        if(!(typeof bfirstName == "string" && typeof blastName == "string" && typeof bemail == "string" && bfirstName.trim().length >0 &&blastName.trim().length >0 && bemail.trim().length >0)){
            return res.status(statusandmsg.badrequest).send({message: statusandmsg.allfieldmandmsg});
        }
             
        else {
            let arr = readFile(pathUser);
            let checkUniqueEmail = arr.filter((ele) => {
                return ele.email == bemail && ele.email != req.user.email;
            });
            if (validator.isEmail(bemail)) {
                if (checkUniqueEmail.length > 0) {
                    res.status(statusandmsg.badrequest).send({ message: statusandmsg.uniqueEmail });
                } else {
                    next();
                }
            } else {
                res.status(statusandmsg.badrequest).send({ message: statusandmsg.invalidemailmsg });
            }
        }

    } catch (error) {
        res.status(statusandmsg.internalservererrorcode).send({ message: error.message });
    }
}

function updateOrdersValidator(req,res,next){
    if(!( req.body.quantity )) return res.status(statusandmsg.badrequest).send({message:statusandmsg.allfieldmandmsg});
    if(!(typeof req.body.quantity == "string"))return res.status(statusandmsg.badrequest).send({message:statusandmsg.typestringerrmsg})
    next();
   

}

 
 
module.exports ={profileUpdateValidator,updateOrdersValidator};