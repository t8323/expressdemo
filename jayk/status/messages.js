let messages = {
    registredmsg:"successfully registerd",
    invalidLogedinmsg:"invalid Credentials",
    profileupdatesuccessmsg:"profile updated sucessfully",
    passchangesucmsg:"password changed successfully",
    wrongoldpassmsg:"wrong password",
    conforimpassnotmatmsg : "confirm Password not matched",
    unauthorizedmsg:"unauthorized access denied",
    usernotfoundmsg:"user not found",
    tokennotfoundmsg:"Token not found",
    allfieldmandmsg:"field types should be string and all fields mandatory",
    uniqueEmail:"email should be unique",
    invalidemailmsg:"invalid email",
    noordersmsg:"You have no orders",
    productsucessmsg:"Product successfully purchased",
    validqtymsg:"Please send valid quantity and productid ",
    prodnotfoundmsg:"Product not found",
    prodoutofstockmsg:"Product out of stock",
    notsufqty:"not suffiecient quantity",
    productFetchedmsg:"Product fetched successfully",
    productaddmsg:"Product added successfully",
    typestringerrmsg:"Types must be a string of all fields",
    logOutmsg:"Loged out successfully",
    logedInmsg:"Loged in successfully",
    isadminerrmsg:"Admin recorces Access denied!",
    buyererrmsg:"Buyer recorces Access denied!",
    sellererrmsg:"Seller recorces Access denied!",
    userfetchedsucmsg:"users fetched successfully",
    userdeletedsucmsg:"User Deleted successfully",
    orderfetchsussessmsg:"Order fetched successfully",
    qtyupdatesucmsg:"quantity updating successfully",
    notloginerrmsg:"You have not loged in",
    invalidtokenmsg:"Invalid Token",
    sessionexpiredmsg:"Session expired please re-login",




};

module.exports = messages;