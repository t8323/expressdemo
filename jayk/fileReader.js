let fs = require('fs');
const path = require('path');
 
 function writeFile(path,data){
    fs.writeFileSync(path,JSON.stringify(data));
 }

 function readFile(path){
   let arr=[];
   if(fs.existsSync(path)){
        arr= JSON.parse(fs.readFileSync( path,"utf-8"));
   }
   return arr;
}

module.exports = {writeFile,readFile};