const message = {
    regsuccess : { message: 'Register Successfully'},
    invalidin : { message: 'Invalid Email and Password'},
    login: 'Login Successfully',
    logout: {message: 'Logout Successfully'},
    proup : { message: 'Profile Change Successfully' },
    passwrong : {message: 'Wrong Password'},
    passnotmatch : {message: 'Password not match'},
    passup : {message: 'Password Change Successfully'},
    exist : {message: 'Email Already Exist'},
    notvale : { message: 'Email is not valid'},
    reqfiedls : { message: 'All Fields required'},
    deny : {message:'Access denied'},
    usernot : {message:'User Not Found'},
    productsuccess : {message:'Product Added Successfully to Orders'},
    no : {message:'You have No orders'},
    noaccess: {message: 'Admin Resource not Access'},
    invalq : { message: 'Invalid Quantity' },
    productnot : { message: 'Product Not Found' },
    invalpr: {message: 'Invalid Price of Product'},
    outofs : { message:  'Product Out Of Stock'},
    notinf :{message:'Product Quantity not Sufficient'},
    productsuc : {message:'Product Added Successfully to Sell'},
    notlogin: {message: 'You have not loged in yet!!!'},  
    invaltkn: {message: 'Invalid Token'},
    sessionexp: {message: 'Session Expired!'},
    nbuyer: { message: 'You are not Buyer' },
    nseller: { message: 'You are not Seller' },
    nadmin: {message:'You are not Admin'},
    udel: {message: 'User Deleted Successfully'}
}


module.exports = message;