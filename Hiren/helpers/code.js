const code ={
    success:200,
    create:201,
    internalservererr:500,
    badrequest:400,
    unauthorized:401,
    forbidden:403,
    notfound:404
}

module.exports = code;