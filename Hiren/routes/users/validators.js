const validators = {}
const validator = require('validator');
const fs = require('fs');
const msg = require('../../helpers');

validators.signUp = (req, res, next) => {
    try {
        const { firstName, lastName, email, password } = req.body;
        if (firstName && lastName && email && password && firstName.trim().length > 0 && lastName.trim().length > 0 && email.trim().length > 0 && password.trim().length > 0) {
            if (validator.isEmail(req.body.email)) {
                if (fs.existsSync("./user.json")) {
                    const arr = JSON.parse(fs.readFileSync("./user.json", "utf-8"));
                    const chk = arr.find((e) => {
                        return e.email === req.body.email;
                    })
                    if (!chk) {
                        next();
                    } else {
                        return res.status(msg.code.badrequest).json(msg.msgs.exist);
                    }
                }
                else {
                    next();
                }
            } else {
               return res.status(msg.code.badrequest).json(msg.msgs.notvale);
            }

        } else {
           return res.status(msg.code.badrequest).json(msg.msgs.reqfiedls);
        }

    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }

}

validators.login = (req, res, next) => {
    try {
        const { email, password } = req.body;
        if (email && password && email.trim().length > 0 && password.trim().length > 0) {
            next();
        } else {
            return res.status(msg.code.badrequest).json(msg.msgs.reqfiedls);
        }
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }

}


validators.profileUpdate = (req, res, next) => {
    try {
        const { firstName, lastName, email } = req.body;
        if (firstName && lastName && email && firstName.trim().length > 0 && lastName.trim().length > 0 && email.trim().length > 0) {
            const arr = JSON.parse(fs.readFileSync('./user.json', 'utf-8'));
            const chk = arr.filter((e) => {
                return e.email === req.body.email && e.email != req.user.email;
            })
            if (validator.isEmail(req.body.email)) {
                if (chk.length >= 1) {
                    res.status(msg.code.badrequest).json(msg.msgs.exist);
                    return;
                } else {
                    next();
                }
            } else {
                return res.status(msg.code.badrequest).json(msg.msgs.notvale);
            }
        } else {
            return res.status(msg.code.badrequest).json(msg.msgs.reqfiedls);
        }
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

validators.changePassword = (req, res, next) => {
    try {
        const { oldpassword, newpassword, confirmnewpassword } = req.body;
        if (!(oldpassword && newpassword, confirmnewpassword && newpassword.trim().length > 0 && confirmnewpassword.trim().length > 0)) {
            return res.status(msg.code.badrequest).json(msg.msgs.reqfiedls);
        }
        next();
    } catch (err) {
        return res.status(msg.code.internalservererr).json({ message: err.message });
    }
}
module.exports = validators