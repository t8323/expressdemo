const controllers = {}
require('dotenv').config();
const fs = require('fs')
const usersarr = require('../../user.json');
const { user } = require('./middlewares');
const jwt = require('jsonwebtoken');
const msg = require('../../helpers');
const bcrypt = require('bcrypt');

controllers.signUp = (req, res) => {
    try {
        if (fs.existsSync('./user.json')) {
            const arr = usersarr;
            req.body.id = Date.now();
            req.body.role = "Buyer";
            req.body.Token = [];
            const salt = bcrypt.genSaltSync(10)
            const hashpass = bcrypt.hashSync(req.body.password, salt);
            req.body.password = hashpass
            arr.push(req.body);
            fs.writeFileSync('./user.json', JSON.stringify(arr));
            return res.status(msg.code.create).json(msg.msgs.regsuccess);
        } else {
            req.body.id = Date.now();
            req.body.Token = [];
            req.body.role = "Buyer";
            const salt = bcrypt.genSaltSync(10)
            const hashpass = bcrypt.hashSync(req.body.password, salt);
            req.body.password = hashpass
            const arr = [req.body];
            fs.writeFileSync('./user.json', JSON.stringify(arr));
            return res.status(msg.code.create).json(msg.msgs.regsuccess);
        }
    } catch (err) {
        return res.status(msg.code.internalservererr).json({ message: err.message });
    }
}

controllers.login = (req, res) => {
    try {
        const arr = usersarr;
        const chkidx = arr.findIndex((e) => {
            return e.email === req.body.email;
        })
        if(chkidx == -1) return res.status(msg.code.notfound).json(msg.msgs.usernot);
        const comparePassword = bcrypt.compareSync(req.body.password, arr[chkidx].password);
        if(!comparePassword) return res.status(msg.code.unauthorized).json(msg.msgs.invalidin) 
            const token = jwt.sign(
                { id: arr[chkidx].id },
                process.env.SERET_KEY,
                { expiresIn: '7200s'}
            );
            if (arr[chkidx].Token.length === 3) arr[chkidx].Token.shift();
            arr[chkidx].Token.push(token);
            fs.writeFileSync('./user.json', JSON.stringify(arr));
            return res.status(msg.code.success).json({message: msg.msgs.login, token});
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

controllers.logout = (req,res) =>{
    const udata = usersarr;
    const logedUserIndex = udata.findIndex((e) => {
        return e.id == req.user.id;
    })
    const tokenIndex = udata[logedUserIndex].Token.findIndex((e) => {
        return e == req.user.token;
    })
    udata[logedUserIndex].Token.splice(tokenIndex, 1);
    fs.writeFileSync('./user.json',JSON.stringify(udata));
    return res.status(msg.code.success).json(msg.msgs.logout);
}

controllers.editProfile = (req, res) => {
    try {
        const arr = usersarr;
        const edit = arr.map((e) => {
            if (e.id === req.user.id) {
                return { ...e, firstName: req.body.firstName, lastName: req.body.lastName, email: req.body.email }
            }
            return e;
        })
        fs.writeFileSync('./user.json', JSON.stringify(edit));
        return res.status(msg.code.create).json(msg.msgs.proup);
    } catch (error) {
        return res.statusCode(msg.code.internalservererr).json({ message: error.message });
    }
}

controllers.changePassword = (req, res) => {
    try {
        const { oldpassword, newpassword, confirmnewpassword } = req.body;
        if (req.body.oldpassword === req.user.password) {
            if (req.body.newpassword === req.body.confirmnewpassword) {
                const arr = usersarr;
                const change = arr.map((e) => {
                    if (e.id === req.user.id) {
                        return { ...e, password: req.body.newpassword }
                    }
                    return e;
                })
                fs.writeFileSync('./user.json', JSON.stringify(change));
                return res.status(msg.code.create).json(msg.msgs.passup);
            } else {
                return res.json(msg.msgs.passnotmatch);
            }
        } else {
            return res.status(msg.code.unauthorized).json(msg.msgs.passwrong);
        }
    } catch (error) {
        return res.statusCode(msg.code.internalservererr).json({ message: error.message });
    }
}

module.exports = controllers