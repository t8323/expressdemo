const express = require('express');
const app = express();
const controllers = require('./controllers');
const middlewares = require('./middlewares');
const validators = require('./validators');


app.post('/register',validators.signUp,controllers.signUp);
app.post('/login',validators.login, controllers.login);
app.post('/editprofile/:userid',  middlewares.authenticate, validators.profileUpdate, controllers.editProfile);
app.post('/changepassword/:userid', middlewares.authenticate, validators.changePassword, controllers.changePassword);
app.get('/logout', middlewares.authenticate, controllers.logout);

module.exports = app;