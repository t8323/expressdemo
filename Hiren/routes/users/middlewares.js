const middlewares = {}
const jwt = require('jsonwebtoken');
const fs = require('fs');
require('dotenv').config();
const usersarr = require('../../user.json');
const msg = require('../../helpers'); 

middlewares.authenticate = (req, res, next) => {
    let token = req.header('authorization') || req.body.token || "";

    const userdata = usersarr;
    try {
        if (!token) return res.status(msg.code.unauthorized).json(msg.msgs.notlogin);
        token = token.slice(7);
        const user = jwt.verify(token, process.env.SERET_KEY);
        const authenticatuser = userdata.find((e) => {
            return user.id == e.id;
        })
        if (!(authenticatuser && authenticatuser.Token.includes(token))) return res.status(msg.code.unauthorized).json(msg.msgs.deny);
        req.user = { ...authenticatuser, token };
        next();
    } catch (error) {
        if (error.message == "jwt expired") {
            const payload = jwt.verify(token, process.env.SERET_KEY, { ignoreExpiration: true });
            const uIndex = userdata.findIndex(e=>e.id == payload.id);
            const tokenIndex = userdata[uIndex].Token.findIndex(e=>e == token);
            if(tokenIndex == -1) return res.status(msg.code.unauthorized).json(msg.msgs.invaltkn);
            userdata[uIndex].Token.splice(tokenIndex,1);
            fs.writeFileSync('./user.json',JSON.stringify(userdata));
            return res.status(msg.code.success).json(msg.msgs.sessionexp);
        } else{
          return  res.status(msg.code.unauthorized).json({ message: error.message });
        }
    }
}  

middlewares.seller = (req, res, next) => {
    try {
        if (req.user.role === "Seller") next();
        else return res.status(msg.code.unauthorized).json(msg.msgs.nseller);
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

middlewares.buyer = (req, res, next) => {
    try {
        if (req.user.role === "Buyer") next();
        else return res.status(msg.code.unauthorized).json(msg.msgs.nbuyer);
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

middlewares.admin = (req, res, next) => {
    try {
        if (req.user.role === "Admin") next();
        else return res.status(msg.code.unauthorized).json(msg.msgs.nadmin);
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

module.exports = middlewares;