const validators = {}
const productsarr = require('../../product.json');
const msg = require('../../helpers');

validators.buyProduct = (req, res, next) => {
    try {
        const { pid, pqty } = req.body;
        const pdata = productsarr;
        const idxofp = pdata.findIndex((e) => e.productid == pid)
        if(!(pid && pqty)) return res.status(msg.code.badrequest).json(msg.msgs.reqfiedls);
        if (!(typeof pid === 'string' && typeof pqty === 'string'&& pid && pqty > 0)) return res.status(msg.code.badrequest).json(msg.msgs.invalq)
        if (idxofp === -1) return res.status(msg.code.badrequest).json(msg.msgs.productnot)
        if (pdata[idxofp].quantity <= 0) return res.status(msg.code.badrequest).json(msg.msgs.outofs);
        if(pdata[idxofp].quantity - pqty <0) return res.status(msg.code.badrequest).json(msg.msgs.notinf)
        else {
           next();
        }
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}


module.exports = validators