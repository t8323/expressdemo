const controllers = {}
const fs = require('fs')
const productsarr = require('../../product.json');
const ordersarr = require('../../order.json');  
const msg = require('../../helpers');

controllers.makeOrder = (req, res) => {
    try {
            const pdata = productsarr;
            const idxofp = pdata.findIndex((e) => {
                return e.id == req.body.id;
            })
            pdata[idxofp].quantity = pdata[idxofp].quantity - req.body.pqty;
            fs.writeFileSync('./product.json', JSON.stringify(pdata));
            if (fs.existsSync('./order.json')) {      
                        const orders = ordersarr;
                        orders.push({ orderid: Date.now(), buyerid: req.user.id, product: pdata[idxofp], buyquantity: req.body.pqty })
                        fs.writeFileSync('./order.json', JSON.stringify(orders));
                        return res.status(msg.code.success).json(msg.msgs.productsuccess);
                    } else {
                        const orders = [{ orderid: Date.now(), buyerid: req.user.id, product: pdata[idxofp], buyquantity: req.body.pqty }];
                        fs.writeFileSync('./order.json', JSON.stringify(orders));
                        return res.status(msg.code.success).json(msg.msgs.productsuccess);
                    }

    }catch (err) {
        return res.status(msg.code.internalservererr).json({ message: err.message });
    }

}

controllers.myOrder = (req, res) => {
    try{
            const orders = ordersarr;
            res.status(msg.code.success).json({'Your order':orders});       
    }catch (err) {
        return res.status(msg.code.internalservererr).json({ message: err.message });
    }   
}


module.exports = controllers