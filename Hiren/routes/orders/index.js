const express = require('express');
const app = express();
const controllers = require('./controllers');
const middlewares = require('../users/middlewares');
const validators = require('./validators');

app.post('/makeorder', middlewares.authenticate, middlewares.buyer, validators.buyProduct, controllers.makeOrder);
app.get('/myorder', middlewares.authenticate, middlewares.buyer, controllers.myOrder);

module.exports = app;