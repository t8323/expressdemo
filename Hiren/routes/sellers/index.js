const express = require('express');
const app = express();
const controllers = require('./controllers');
const middlewares = require('../users/middlewares');
const validators = require('./validators');


app.post('/sellproduct', middlewares.authenticate, middlewares.seller, validators.sellproduct, controllers.sellproduct);
app.get('/myproduct',middlewares.authenticate, middlewares.seller,controllers.showproduct);

module.exports = app;