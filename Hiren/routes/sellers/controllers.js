const controllers = {};
const productsarr = require('../../product.json');
const fs = require('fs');
const msg = require('../../helpers');

controllers.sellproduct = (req, res) => {
    try {
        if (fs.existsSync('./product.json')) {
            const products = productsarr;
            products.push({ ...req.body, productid: Date.now(), sellerid: req.user.id });
            fs.writeFileSync('./product.json', JSON.stringify(products));
            return res.status(msg.code.create).json(msg.msgs.productsuc);
        } else {
            const products = [{ ...req.body, productid: Date.now(), sellerid: req.user.id }];
            fs.writeFileSync('./product.json', JSON.stringify(products));
            return res.status(msg.code.create).json(msg.msgs.productsuc);
        }
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

controllers.showproduct = (req, res) => {
    try {
        let products = productsarr;
        products = products.filter((ele)=>{
            return ele.sellerid == req.user.id;
        })
        return res.status(msg.code.success).json({'Your Sell Products':products})
       
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

module.exports = controllers;