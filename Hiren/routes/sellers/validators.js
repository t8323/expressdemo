const validators = {};
const msg = require('../../helpers');
validators.sellproduct = (req,res,next) => {
    try{
    const {productname,quantity,price} = req.body;
    if(!(productname && quantity && price)) return res.status(msg.code.badrequest).json(msg.msgs.reqfiedls);
    if(typeof productname === 'string' && productname.trim().length < 0) return res.status(msg.code.badrequest).json(msg.msgs.invalp);
    if(typeof quantity === 'string' && quantity.trim().length < 0) return res.status(msg.code.badrequest).json(msg.msgs.invalq);
    if(price.trim().length < 0 ) return res.status(msg.code.badrequest).json(msg.msgs.invalpr);
    next();
    } catch(error){
        res.status(msg.code.internalservererr).json({ message: error.message });
    }
} 

module.exports = validators;