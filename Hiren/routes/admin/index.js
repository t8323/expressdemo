const express = require('express');
const app = express();
const controllers = require('./controllers');
const middlewares = require('../users/middlewares');
const validators = require('./validators');

app.get('/getusers', middlewares.authenticate,middlewares.admin, controllers.allUser);
app.post('/edituser/:userid',middlewares.authenticate, middlewares.admin,validators.allprofileUpdate, controllers.editUser);
app.get('/deleteuser/:userid', middlewares.authenticate, middlewares.admin, controllers.deleteUser);
app.get('/showallorder',middlewares.authenticate, middlewares.admin, controllers.allUserorder);

module.exports = app;