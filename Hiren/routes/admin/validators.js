const validators = {}
const validator = require('validator');
const fs = require('fs');
const msg = require('../../helpers');

validators.allprofileUpdate = (req, res, next) => {
    try {
        const { firstName, lastName, email } = req.body;
        if (firstName && lastName && email && firstName.trim().length > 0 && lastName.trim().length > 0 && email.trim().length > 0) {
            const arr = JSON.parse(fs.readFileSync('./user.json', 'utf-8'));
            const chk = arr.filter((e) => {
                return e.email === req.body.email && e.email != req.user.email;
            })
            if (validator.isEmail(req.body.email)) {
                if (chk.length >= 1) {
                    res.status(msg.code.badrequest).json(msg.msgs.exist);
                    return;
                } else {
                    next();
                }
            } else {
                return res.status(msg.code.badrequest).json(msg.msgs.notvale);
            }
        } else {
            return res.status(msg.code.badrequest).json(msg.msgs.reqfiedls);
        }
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

module.exports = validators;