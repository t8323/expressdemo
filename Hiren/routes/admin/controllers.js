const controllers = {}
const usersarr = require('../../user.json');
const ordersarr = require('../../order.json');
const msg = require('../../helpers');
const fs = require('fs');

controllers.allUser = (req, res) => {
    const users = usersarr.filter(e => e.role != "Admin");
    return res.status(msg.code.success).json({ 'Registerd User': users });
}

controllers.editUser = (req, res) => {
    try {
        const udata = usersarr;
        const uIndex = udata.findIndex((ele) => {
            return ele.id == req.params.userid;
        })
        if (uIndex == -1) return res.status(msg.code.notfound).json(msg.msgs.usernot);
        udata[uIndex] = { ...udata[uIndex], firstName: req.body.firstName, lastName: req.body.lastName, email: req.body.email };
        fs.writeFileSync('./user.json', JSON.stringify(udata));
        return res.status(msg.code.success).json(msg.msgs.proup);
    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

controllers.deleteUser = (req, res) => {
    try {
        const udata = usersarr;
        const uIndex = udata.findIndex((ele) => {
            return ele.id == req.params.userid;
        })
        if (uIndex == -1) return res.status(msg.code.notfound).json(msg.msgs.usernot);
        udata.splice(uIndex, 1);
        fs.writeFileSync('./user.json', JSON.stringify(udata));
        return res.status(msg.code.success).json(msg.msgs.udel);

    } catch (error) {
        return res.status(msg.code.internalservererr).json({ message: error.message });
    }
}

controllers.allUserorder = (req, res) => {
    if (fs.existsSync("./orders.json")) {
        const orders = ordersarr;
        return res.status(msg.code.success).json({ 'All Orders': orders })
    }
}

module.exports = controllers;