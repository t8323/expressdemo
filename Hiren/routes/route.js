const app = require('express').Router();
const users = require('./users');
const orders = require('./orders');
const admin = require('./admin');
const sellers = require('./sellers');

app.use('/users',users);
app.use('/orders',orders)
app.use('/admin',admin);
app.use('/sell',sellers);

app.all('*', (req, res) => {
    res.send('Route not found')
})
module.exports = app;