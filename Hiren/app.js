require('dotenv').config();
const express = require('express');
const app = express();
const routes = require('./routes/route');
const port = process.env.PORT;
app.use(express.json());
app.use('/',routes);

app.listen(port,()=>{
    console.log(`Server is running on port ${port}`);
})