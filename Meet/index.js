const express = require("express")
const app = express()
const routes = require("./routes/route")
const helmet = require("helmet")
const cors = require("cors")
const bodyParser = require("body-parser")
require("dotenv").config()
app.use(cors())
app.use(helmet())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use("/",routes)

app.listen(process.env.PORT,()=>{
    console.log(`Server is listening on port no. ${process.env.PORT}`);
})