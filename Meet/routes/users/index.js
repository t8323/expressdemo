const router = require("express").Router()
const controllers = require("./libs/controller")
const middleware = require("./libs/middleware")
const validator = require("./libs/validator")


router.post("/signup",validator.signup,controllers.signup)
router.post("/login",middleware.login,validator.login,controllers.login)
router.post("/changepassword",middleware.authentication,validator.changepassword,controllers.changepassword)
router.post("/updateprofile",middleware.authentication,validator.updateProfile,controllers.updateProfile)
router.get("/profile",middleware.authentication,controllers.getProfile)
router.post("/logout",middleware.authentication,controllers.logout)
router.post("/addproduct",middleware.sellerAuthentication,validator.addProduct,controllers.addProduct)
router.get("/stockProducts",middleware.sellerAuthentication,controllers.stockProducts)
router.get("/orderedproducts",middleware.sellerAuthentication,controllers.ordersProducts)
module.exports = router