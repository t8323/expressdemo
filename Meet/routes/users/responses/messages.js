const messages = {
    fieldsRequired : "All fields are required",
    invalidPassword :"Invalid password",
    bothPasswordNotMatch :"Both password doesn't match",
    tokenIsNotSet :"Token is not set",
    tokenIsInvalid :"Token is no longer valid",
    alreadyRegistreted : " is already registreted",
    registreted :"Account successfully created",
    wrongPassword:"Please enter a correct password",
    useRegistretedEmail : "Please use registreted email id",
    updatedSuccessfully:"Updated successfully",
    serverError :"Internal server error",
    loggedOut:"Successfully logged out",
    loginRequired :"Login Required",
    unauthorized:"Unauthorized request",
    addedSuccessfully:"Added successfully",
    notFound:"Not Found"
}

module.exports = messages