require("dotenv").config()
const middleware = {}
const {status,messages } = require("../responses")
const jwt = require("jsonwebtoken")
const path = require("path")
const userData = require("../data/user.json")
const userPath = path.join(__dirname,"../data/user.json")

middleware.signup = (req,res,next)=>{

    next()
}
middleware.login = (req,res,next)=>{
    
    next()
}
middleware.authentication = (req,res,next)=>{
    const authHeader = req.headers['authorization']
    const token  = authHeader &&  authHeader.split(" ")[1]
    if(token == null) 
        return res.status(status.badRequest).json({message:messages.tokenIsNotSet})
    jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,user)=>{
        if(err)
            return res.status(status.forbidden).json({message:messages.tokenIsInvalid})
        if(userData[`${user.field}`].tokens.length === 0)
            return res.status(status.forbidden).json({error:messages.loginRequired})
        req.user = user.field
        next()
    })
}

middleware.sellerAuthentication = (req,res,next)=>{
    const authHeader = req.headers['authorization']
    const token  = authHeader &&  authHeader.split(" ")[1]
    if(token == null) 
        return res.status(status.badRequest).json({message:messages.tokenIsNotSet})
    jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,user)=>{
        if(err)
            return res.status(status.forbidden).json({message:messages.tokenIsInvalid})
        if(userData[`${user.field}`].tokens.length === 0)
            return res.status(status.forbidden).json({error:messages.loginRequired})
        if(!userData[`${user.field}`].role)
            return res.status(status.unauthorized).json({error:messages.unauthorized})
        req.user = user.field
        req.sellerId = userData[`${user.field}`].sellerId
        next()
    })
}
module.exports = middleware