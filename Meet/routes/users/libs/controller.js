require("dotenv").config()
const fs = require("fs")
const jwt = require("jsonwebtoken")
const path = require("path")
const userPath = path.join(__dirname, "../data/user.json")
const productPath = path.join(__dirname,"../../orders/data/products.json")
let userData = require("../data/user.json")
let productData = require("../../orders/data/products.json")
let orderData = require("../../orders/data/orders.json")
const { status, messages } = require("../responses")
const crypto = require("crypto")
const {generateAccessToken} = require("../../helper")
// test
class User {
    signup = (req, res) => {
        const { firstName, lastName, emailId, password,userRole } = req.body
        let objKeys = Object.keys(userData)
        let user
        if (objKeys.findIndex((ele) => ele === emailId) >= 0)
        return res.status(status.badRequest).json({ message: `${emailId} ${messages.alreadyRegistreted}` })
        const sHashPassword = crypto.createHmac("sha512",process.env.SECRET_KEY).update(password).digest("hex")
        if(userRole === "Seller"){
            const sellerId = new Date().getTime()
            user = {
                firstName: firstName,
                lastName: lastName,
                emailId: emailId,
                role:userRole,
                sellerId:sellerId,
                password: sHashPassword,
                orders: [],
                tokens:[]
            }
        }
        else{
            user = {
                firstName: firstName,
                lastName: lastName,
                emailId: emailId,
                password: sHashPassword,
                orders: [],
                tokens:[]
            }
        }
        userData[`${emailId}`] = user
        try {
            fs.writeFileSync(userPath, JSON.stringify(userData))
        }
        catch (err) {
            return res.status(status.internalServerError).json({ err: messages.serverError })
        }
        return res.status(status.ok).json({ message: messages.registreted })
    }
    login = (req, res) => {
        const { emailId, password } = req.body
        let objKeys = Object.keys(userData)
        const sHashPassword = crypto.createHmac("sha512",process.env.SECRET_KEY).update(password).digest("hex")
        if (objKeys.findIndex((ele) => ele === emailId) < 0)
            return res.status(status.badRequest).json({ message: messages.useRegistretedEmail })
        if (userData[`${emailId}`].password === sHashPassword) {
            const accessToken = generateAccessToken(emailId)
            const tokens = userData[`${emailId}`].tokens
            if(userData[`${emailId}`].tokens.length >2){
                tokens.unshift()
                tokens.shift(accessToken)
            }
            tokens.push(accessToken)
            userData[`${emailId}`].tokens = tokens
            try {
                fs.writeFileSync(userPath,JSON.stringify(userData))
            }
            catch(err){
                return res.status(status.internalServerError).json({error:messages.serverError})
            }
            return res.status(status.ok).json({ message: accessToken })
        }
        return res.status(status.forbidden).json({ message: messages.wrongPassword })
    }
    changepassword = (req, res) => {
        const email = req.user
        const { currentPassword, newPassword } = req.body
        const sHashPassword = crypto.createHmac("sha512",process.env.SECRET_KEY).update(currentPassword).digest("hex")
        const sNewHashPassword = crypto.createHmac("sha512",process.env.SECRET_KEY).update(newPassword).digest("hex")
        if (userData[`${email}`].password !== sHashPassword)
            return res.status(status.forbidden).json({ message: messages.invalidPassword })
        userData[`${email}`].password = sNewHashPassword
        try {
            fs.writeFileSync(userPath, JSON.stringify(userData))
        }
        catch (err) {
            return res.status(status.internalServerError).json({ error: messages.serverError })
        }
        return res.status(status.ok).json({ message: messages.updatedSuccessfully })
    }
    updateProfile = (req, res) => {
        const email = req.user
        const { emailId, firstName, lastName } = req.body
        let accessToken = ""
        userData[`${email}`].firstName = firstName
        userData[`${email}`].lastName = lastName
        if (userData[`${email}`].email != emailId) {
            userData[`${email}`].emailId = emailId
            userData[`${emailId}`] = userData[`${email}`]
            delete userData[`${email}`]
            accessToken = jwt.sign(emailId, process.env.ACCESS_TOKEN_SECRET)
            console.log("new access token", accessToken);
        }
        try {
            fs.writeFileSync(userPath, JSON.stringify(userData))
        }
        catch (err) {
            return res.status(status.internalServerError).json({ error: messages.serverError })
        }
        return res.status(status.ok).json({ message: ` ${messages.updatedSuccessfully}` })
    }
    getProfile = (req, res) => {
        if (userData[`${req.user}`])
            return res.status(status.ok).json({
                Name: `${userData[`${req.user}`].firstName} ${userData[`${req.user}`].lastName}`,
                EmailId: userData[`${req.user}`].emailId,
            })
        return res.status(status.internalServerError).json({ message: messages.serverError })
    }
    logout = (req,res)=>{ 
        const semailId = req.user
        const authHeader = req.headers['authorization']
        const token  = authHeader &&  authHeader.split(" ")[1]
        let tokens = userData[`${semailId}`].tokens
        console.log(userData[`${semailId}`].tokens);
        for(let i=0;i<tokens.length;i++){
            console.log("called");
            if(tokens[i]===token){
                console.log("called");
                tokens.splice(i,1)
            }
        }
        userData[`${semailId}`].tokens = tokens
        console.log(userData[`${semailId}`].tokens);
        try {
            fs.writeFileSync(userPath, JSON.stringify(userData))
        }
        catch (err) {
            return res.status(status.internalServerError).json({ error: messages.serverError })
        }
        return res.status(status.ok).json({ message: ` ${messages.loggedOut}` })
    }
    addProduct = (req,res)=>{
        const {productName,quantity} = req.body
        let productKey = Object.keys(productData).length+1
        let product ={
            productName:productName,
            stock:quantity,
            sellerId:req.sellerId
        }
        productData[`${productKey}`]= product
        try{
            fs.writeFileSync(productPath,JSON.stringify(productData))
        }
            catch (err) {
                return res.status(status.internalServerError).json({ err: messages.serverError })
            }
        
        return res.status(status.ok).json({message:messages.addedSuccessfully})
    }
    stockProducts = (req,res)=>{
        const productKeys = Object.keys(productData)
        const result =  productKeys.filter(ele =>productData[ele].sellerId === req.sellerId)
        const output = {}
        if(result.length === 0)
            return res.status(status.forbidden).json({message:messages.notFound})
        for(let i=0;i<result.length;i++){
            output[`${result[i]}`] =productData[result[i]]
        }
        return res.status(status.ok).json(output)
    }
    ordersProducts = (req,res)=>{
        const productKeys = Object.keys(orderData)  
        const result =  productKeys.filter(ele =>orderData[ele].sellerId === req.sellerId)
        const output = {}
        if(result.length === 0)
            return res.status(status.forbidden).json({message:messages.notFound})
        for(let i=0;i<result.length;i++){
            output[`${result[i]}`] =orderData[result[i]]
        }
        return res.status(status.ok).json(output)
    }
}
module.exports = new User
