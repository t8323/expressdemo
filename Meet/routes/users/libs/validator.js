const validator = {}
const {status,messages } = require("../responses")
const { check, validationResult } = require("express-validator")

validator.signup = (req, res, next) => {
    const { firstName, lastName, emailId, password } = req.body
    check("emailId","Invalid email id").isEmail()
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        console.log("called");
        return res.status(400).json({errors:errors})
    }
    const regex = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{8,15}$/g
    if (!firstName || !lastName || !emailId || !password) {
        return res.status(status.badRequest).json({ message: messages.fieldsRequired })
    }
    if (regex.test(password) == false) {
        return res.status(status.badRequest).json({ message: messages.invalidPassword})
    }
    next()
}
validator.login = (req, res, next) => {
    const { emailId, password } = req.body
    if (!emailId || !password) {
        return res.status(status.badRequest).json({ message: messages.fieldsRequired})
    }
    next()
}
validator.changepassword = (req, res, next) => {
    const { currentPassword, newPassword, confirmPassword } = req.body
    const regex = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{8,15}$/g
    if (!currentPassword || !newPassword || !confirmPassword)
        return res.status(status.badRequest).json({ message: messages.fieldsRequired })
    if (regex.test(newPassword) == false)
        return res.status(status.badRequest).json({ message: messages.invalidPassword })
    if (newPassword !== confirmPassword)
        return res.status(403).json({ message: messages.bothPasswordNotMatch })
    next()
}
validator.updateProfile = (req, res, next) => {
    const { emailId, firstName, lastName } = req.body
    if (!emailId || !firstName || !lastName)
        return res.status(status.badRequest).json({ message: messages.fieldsRequired })
    next()
}
validator.addProduct = (req,res,next)=>{
    const {productName,quantity} = req.body
    if(!productName || !quantity)
        return res.status(status.badRequest).json({ message: messages.fieldsRequired })
    next()
}
module.exports = validator