const routers = require("express").Router()
const user = require("./users")
const orders = require("./orders")
const admin = require("./admin")

routers.use("/users",user)
routers.use("/orders",orders)
routers.use("/admin",admin)

routers.all("*",(req,res)=>{
    res.json("Router not found")
})

module.exports = routers
