const status = {
    badRequest : 400,
    forbidden : 403,
    ok:200,
    internalserverError:500
}
module.exports = status