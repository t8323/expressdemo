const messages = {
    tokenIsNotSet :"Token is not set",
    tokenIsInvalid :"Token is no longer valid",
    invalidEmailId :"Please enter a valid email id",
    allFieldsAreRequired :"All fields are required",
    invalidPassword:"Invalid Password",
    alreadyRegistreted : " is already registreted",
    bothPasswordNotMatch :"Both password doesn't match",
    useRegistretedEmail : "Please use registreted email id",
    updateSuccessfully :"Updated successfully",
    emailNotFound :"Sorry, we cannot find this email id",
    internalServerError:"Internal server error",
    deleted:"Deleted successfully",
    invalidOrderId :"Invalid order Id",
    oidRequired : "Order id is required",
    nothingToChange :"There is nothing to change",
}
module.exports = messages