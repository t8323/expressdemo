const messages = {
    fieldsRequired : "All fields are required",
    tokenIsNotSet :"Token is not set",
    tokenIsInvalid :"Token is no longer valid",
    invalidInput :"Invalid input",
    internalServerError : "Internal server error",
    successfullOrder :"Thank you for shopping with us your order has been placed",
    outOfStock:"Sorry item is out of stock we will notify you when product will be available",
    notAvailable :"Sorry, we can't find your requested product",
    noOrderHistory:"You haven't order anything yet"
}

module.exports = messages