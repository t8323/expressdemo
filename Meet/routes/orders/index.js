const router = require("express").Router()
const middleware = require("./libs/middleware")
const validator = require("./libs/validator")
const controller = require("./libs/controller")

router.post("/orderplacing",middleware.authentication,validator.orderPlacing,controller.orderPlacing)
router.get("/orderhistory",middleware.authentication,controller.orderHistory)

module.exports = router