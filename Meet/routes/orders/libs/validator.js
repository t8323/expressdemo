const validator = {}
const {status,messages} = require("../responses")

validator.orderPlacing = (req,res,next)=>{
    const {productName,quantity,saddress} = req.body
    const regex = /[0-9]$/g
    if(!productName || !quantity || !saddress)
        return res.status(status.badRequest).json({message:messages.fieldsRequired})
    if(regex.test(quantity)==false)
        return res.status(status.badRequest).json({message:messages.invalidInput})
    next()
}

module.exports = validator