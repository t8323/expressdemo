const fs = require("fs")
const path = require("path")
const { status, messages } = require("../responses")
const products = path.join(__dirname, "../data/products.json")
const orders = path.join(__dirname, "../data/orders.json")
const user = path.join(__dirname, "../../users/data/user.json")
let userData = require("../../users/data/user.json")
let orderData = require("../data/orders.json")
let productData = require("../data/products.json")
class Orders {
    orderPlacing = (req, res) => {
        const { productName, quantity ,saddress} = req.body
            let objKeys = Object.keys(productData)
            let productId;
            if(objKeys.findIndex((ele)=>productData[ele].productName == productName)<0)
                return res.status(status.forbidden).json({ message: messages.notAvailable })
            for (let i = 0; i < objKeys.length; i++) {
                if (productData[i].productName === productName) {
                    if (productData[i].stock > 0 && productData[i].stock >= quantity) {
                        productId = objKeys[i]
                        productData[i].stock = productData[i].stock - quantity
                        productData = JSON.stringify(productData)
                        try {
                            fs.writeFileSync(products, productData)
                        }
                        catch (err) {
                            return res.status(status.internalServerError).json({ message: messages.internalServerError })
                        }
                        let oArray = userData[req.user].orders
                        let date = new Date()
                        let oId = date.getTime()
                        oArray.push(oId)
                        userData[req.user].orders = oArray
                        let order = {
                            productId: productId,
                            productName: productName,
                            quantity: quantity,
                            orderDate: `${date.getDate()}-${date.getMonth()+1}-${date.getFullYear()}`,
                            oadddress :saddress
                        }
                        orderData[`${oId}`] = order
                        try {
                            fs.writeFileSync(orders, JSON.stringify(orderData))
                        }
                        catch (err) {
                            return res.status(status.internalServerError).json({ message: messages.internalServerError })
                        }
                        try {
                            fs.writeFileSync(user, JSON.stringify(userData))
                        }
                        catch (err) {
                            return res.status(status.internalServerError).json({ message: messages.internalServerError })
                        }
                        return res.status(status.ok).json({ message: messages.successfullOrder })
                    }
                    else {
                        return res.status(status.forbidden).json({ message: messages.outOfStock })
                    }
                }
            }
            // return res.status(status.forbidden).json({ message: messages.notAvailable })
    }
    orderHistory = (req, res) => {
        let orderIds = userData[req.user].orders
        let orderDisplay = []
        if (orderIds.length === 0)
            return res.status(status.forbidden).json({ message: messages.noOrderHistory })
        for (let i = 0; i < orderIds.length; i++) {
            orderDisplay.push(orderData[`${orderIds[i]}`])
        }
        return res.status(status.ok).json({ orders: orderDisplay })
    }
}

module.exports = new Orders