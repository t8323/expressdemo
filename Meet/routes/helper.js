require("dotenv").config()
const jwt = require("jsonwebtoken")
const helper = {}


helper.authentication = (token )=>{
    jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,user)=>{
        if(err)
            return
        return user
        
    })
}
helper.isEmail = (semailId)=>{
    const regex = /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/
    if(regex.test(semailId)==false)
        return false
    return true
}

helper.isPassword = (spassword)=>{
    const regex = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{8,15}$/g
    if (!regex.test(spassword)) 
        return false
    return true
}

helper.generateAccessToken = (field)=>{
   return jwt.sign({field}, process.env.ACCESS_TOKEN_SECRET,{expiresIn:"5h"})
}

helper.auth
module.exports = helper
