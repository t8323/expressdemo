const router = require("express").Router()
const controller = require("./libs/controller")
const middleware = require("./libs/middleware")
const validator = require("./libs/validator")

router.post("/login",validator.login,controller.login)
router.get("/usersdata",middleware.authentication,controller.getUserData)
router.put("/updateuser",validator.updateUserData,middleware.authentication,controller.updateUserData)
router.delete("/deleteuser",validator.deleteUser,middleware.authentication,controller.deleterUser)
router.get("/allorders",middleware.authentication,controller.allorders)
router.put("/updateorder",middleware.authentication,validator.updateOrder,controller.updateOrder)
router.delete("/deleteorder",middleware.authentication,validator.deleteOrder,controller.deleterOrder)

module.exports = router