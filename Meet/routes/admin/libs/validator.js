const validator = {}
const {status,messages} = require("../../responses")
const {isEmail,isPassword} = require("../../helper")

validator.login = (req,res,next)=>{
    const {semailId,spassword} = req.body
    if(!semailId || !spassword)
        return res.status(status.badRequest).json({ message:messages.allFieldsAreRequired })
    if(!isEmail(semailId))
        return res.status(status.badRequest).json({err:messages.invalidEmailId})
    next()
}
validator.updateUserData = (req,res,next)=>{
    const {semailId,sfirstName,slastName} = req.body
    if(!semailId || !sfirstName || !slastName)
        return res.status(status.badRequest).json({ message:messages.allFieldsAreRequired })
    if(!isEmail(semailId))
        return res.status(status.badRequest).json({err:messages.invalidEmailId})
    next()
}
validator.deleteUser = (req,res,next)=>{
    const {semailId} = req.body
    if(!semailId)
        return res.status(status.badRequest).json({ message:messages.allFieldsAreRequired })
    next()
}
validator.updateOrder = (req,res,next)=>{
    const {oId,quantity,saddress} = req.body
    if(!oId)
        return res.status(status.badRequest).json({ message:messages.oidRequired})
    next()
}
validator.deleteOrder = (req,res,next)=>{
    if(!req.body.oId)
        return res.status(status.badRequest).json({ message:messages.oidRequired})
    next()   
}
module.exports = validator