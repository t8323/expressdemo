require("dotenv").config()
const fs = require("fs")
const path = require("path")
const adminData = require("../data/admin.json")
const userData = require("../../users/data/user.json")
const orderData = require("../../orders/data/orders.json")
const productData = require("../../orders/data/products.json")
const userPath = path.join(__dirname , "../../users/data/user.json")
const orderPath = path.join(__dirname, "../../orders/data/orders.json")
const {status,messages} = require("../../responses")
const {generateAccessToken} = require("../../helper")
class Admin {
    login = (req,res)=>{
        const {semailId,spassword} = req.body
        const objKeys = Object.keys(adminData)
        if(objKeys.findIndex((ele)=>ele===semailId)<0)
            return res.status(status.badRequest).json({error:messages.useRegistretedEmail})
        if(adminData[semailId].password === spassword){
            const accessToken = generateAccessToken(semailId)
            return res.status(status.ok).json({ message: accessToken })
        }
        return res.status(status.forbidden).json({error:messages.invalidPassword})
        
    }
    getUserData = (req,res)=>{
        const adminKeys = Object.keys(adminData)
        if(adminKeys.findIndex((ele)=>ele===req.admin)<0)
            return res.status(status.badRequest).json({error:messages.useRegistretedEmail})
        return res.json(userData)
    }
    updateUserData = (req,res)=>{
        const {semailId,sfirstName,slastName} = req.body
        const adminKeys = Object.keys(adminData)
        const userKeys = Object.keys(userData)
        if(adminKeys.findIndex((ele)=>ele===req.admin)<0)
            return res.status(status.badRequest).json({error:messages.useRegistretedEmail})
        if(userKeys.findIndex((ele)=>ele === semailId)<0)
            return res.status(status.badRequest).json({error:messages.emailNotFound})
        
            userData[`${semailId}`].firstName = sfirstName
            userData[`${semailId}`].lastName = slastName
            try{
                fs.writeFileSync(userPath,JSON.stringify(userData))
            }
            catch(err){
                return res.status(status.internalServerError).json({ error: messages.serverError })
            }
            return res.status(status.ok).json({messages:messages.updateSuccessfully})
    }
    deleterUser = (req,res)=>{
        let userKeys = Object.keys(userData)
        if(userKeys.findIndex((ele)=>ele=== req.body.semailId)<0)
            return res.status(status.forbidden).json({message:messages.emailNotFound})
        delete userData[`${req.body.semailId}`]
        try{
            fs.writeFileSync(userPath,JSON.stringify(userData))
        }
        catch(err){
            return res.status(status.internalserverError).json({message:messages.internalServerError})
        }
        return res.status(status.ok).json({message:messages.deleted})
    }
    allorders = (req,res)=>{
        const {semailId,oId} = req.body
        if(!oId)
            return res.status(status.ok).json({data:orderData})
        const orderKeys = Object.keys(orderData)
        if(orderKeys.findIndex((ele)=>ele===oId)<0)
            return res.status(status.badRequest).json({message:messages.invalidOrderId})
        return res.status(status.ok).json({data:orderData[`${oId}`]})
    }
    updateOrder = (req,res)=>{
        const {oId,quantity,saddress} = req.body
        const orderKeys = Object.keys(orderData)
        if(orderKeys.findIndex((ele)=>ele===oId)<0)
            return res.status(status.badRequest).json({message:messages.invalidOrderId})
        if(!quantity && !saddress)
            return res.status(status.badRequest).json({error:messages.nothingToChange})
        if(quantity)
            orderData[`${oId}`].quantity = quantity
        if(saddress)
            orderData[`${oId}`].oadddress = saddress
        console.log(orderData[`${oId}`]);
        try{
            fs.writeFileSync(orderPath,JSON.stringify(orderData))
        }
        catch(err){
            return res.status(status.internalserverError).json({ error: messages.internalServerError })
        }
        return res.status(status.ok).json({messages:messages.updateSuccessfully})
    }
    deleterOrder = (req,res)=>{
        let orderKeys = Object.keys(orderData)
        if(orderKeys.findIndex((ele)=>ele === req.body.oId)<0)
            return res.status(status.forbidden).json({error:messages.invalidOrderId})
        delete orderData[`${req.body.oId}`]
        try{
            fs.writeFileSync(orderPath,JSON.stringify(orderData))
        }
        catch(err){
            return res.status(status.internalserverError).json({message:messages.internalServerError})
        }
        return res.status(status.ok).json({message:messages.deleted})
    }
}
module.exports = new Admin