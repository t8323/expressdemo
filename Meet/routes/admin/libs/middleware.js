require("dotenv").config()
const middleware = {}
const jwt = require("jsonwebtoken")
const {status,messages} = require("../../responses")

middleware.authentication = (req,res,next)=>{
    const authHeader = req.headers['authorization']
    const token  = authHeader &&  authHeader.split(" ")[1]
    if(token == null) 
        return res.status(status.badRequest).json({message:messages.tokenIsNotSet})
    jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,admin)=>{
        if(err)
            return res.status(status.forbidden).json({message:messages.tokenIsInvalid})
        req.admin = admin.field
        next()
    })
}

module.exports = middleware