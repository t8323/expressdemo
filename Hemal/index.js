const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const routes = require('./routes/route')
require('dotenv').config()

const port = process.env.PORT

app.use(bodyParser.json())

app.use('/api/v1', routes)

app.listen(port, () => {
    console.log("Listening on", port);
})