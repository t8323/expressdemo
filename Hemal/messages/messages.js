const messages = {
    successfullRegMsg: { messaege: 'Successfully registered' },
    alreadyExistMsg: { message: 'User already exist' },
    notRegisteredMsg: { message: 'User not registered' },
    invalidPasswordMsg: { message: 'Password is invalid' },
    loginSuccessMsg: { message: 'Login Successfully' },
    passwordChangedMsg: { message: 'Password changed Successfully' },
    profileUpadatedMsg: { message: 'Profile updated Successfully' },
    firstNameReqMsg: { message: 'firstName is required' },
    lastNameReqMsg: { message: 'lastName is required' },
    userTypeReqMsg: { message: 'User Type is required' },
    emailReqMsg: { message: 'email is missing or invalid' },
    passwordReqMsg: { message: 'password is required' },
    productUnavailableMsg: { message: 'Product is not available' },
    productOutOfStockMsg: { message: 'This product is out of stock' },
    successfullOrderMsg: { message: 'Order is confirmed' },
    productNameReqMsg: { message: 'Product name is missing' },
    productQuantityReqMsg: { message: 'Quantity of product is missing or invalid' },
    productPriceReqMsg: { message: 'Price per product is missing' },
    userTypeReqMsg: { message: 'User role is missing' },
    idReqMsg: { message: 'Id is required' },
    logoutSuccessMsg: { message: 'Logged out successfully' },
    profileDelSuccessMsg: { message: 'Profile deleted successfully' },
    needToLoginFirstMsg: { message: 'You need to login first' },
    notFoundMsg: { message: 'Not found' }
}

module.exports = messages