const status = {
    success: 200,
    forbidden: 403,
    unauthorized: 401,
    not_acceptable: 406,
    
}

module.exports = status