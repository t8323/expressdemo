const app = require('express').Router()
const user = require('./user/index')
const order = require('./orders/index')
const admin = require('./admin/index')

app.use('/user', user)
app.use('/order', order)
app.use('/admin', admin)

app.all('*', (req, res) => {
    res.send("Route not found")
})

module.exports = app