const controllers = {}
const user = require('../user/data.json')
const products = require('../orders/products.json')
const jwt = require('jsonwebtoken')
const messages = require('../../messages/index')
const utils = require('../../utils')
require('dotenv').config()
const secretKey = process.env.SECRET_KEY

controllers.editProfile = (req, res) => {
    const { firstName, lastName, email, userType } = req.body
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.status(messages.status.forbidden).json({ err })
        } else {
            if (user[user.findIndex(e => e.email === authData.email)].userType === 'Admin') {
                for (let key in user) {
                    if (user[key].email === email) {
                        user[key].firstName = firstName
                        user[key].lastName = lastName
                        user[key].email = email
                        user[key].userType = userType
                    }
                }
                utils.writeFile('./routes/user/data.json', user)
                return res.status(messages.status.success).json(messages.messages.profileUpadatedMsg)
            } else {
                return res.status(messages.status.unauthorized).json({ message: 'Only Admin can delete other\'s profile' })
            }
        }
    })
}

controllers.deleteProfile = (req, res) => {
    const { email } = req.body
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.status(messages.status.forbidden).json({ err })
        } else {
            if (user[user.findIndex(e => e.email === authData.email)].userType === 'Admin') {
                const keepProfiles = user.filter(e => e.email !== email)
                const keepProducts = products.filter(e => e.email !== email)

                if(keepProfiles.length === user.length) return res.status(messages.status.unauthorized).json(messages.messages.notRegisteredMsg)

                utils.writeFile('./routes/user/data.json', keepProfiles)
                utils.writeFile('./routes/orders/products.json', keepProducts)
                return res.status(messages.status.success).json(messages.messages.profileDelSuccessMsg)
            } else {
                return res.status(messages.status.unauthorized).json({ message: 'Only Admin can delete other\'s profile' })
            }
        }
    })
}

controllers.editProduct = (req, res) => {
    const { id, productName, quantity, pricePP } = req.body
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.status(messages.status.forbidden).json({ err })
        } else {
            if (user[user.findIndex(e => e.email === authData.email)].userType === 'Admin') {
                for (let key in products) {
                    if (products[key].id === id) {
                        products[key].productName = productName
                        products[key].quantity = quantity
                        products[key].pricePP = pricePP
                    }
                }
                utils.writeFile('./routes/orders/products.json', products)
                return res.status(messages.status.success).json({ message: 'Product updated successfully' })
            } else {
                return res.status(messages.status.unauthorized).json({ message: 'Only Admin can delete other\'s profile' })
            }
        }
    })
}

controllers.deleteProduct = (req, res) => {
    const { id } = req.body
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.status(messages.status.forbidden).json({ err })
        } else {
            if (user[user.findIndex(e => e.email === authData.email)].userType === 'Admin') {
                const keepProducts = products.filter(e => e.id !== id)

                if(keepProducts.length === products.length) return res.status(messages.status.unauthorized).json(messages.messages.notFoundMsg)

                utils.writeFile('./routes/orders/products.json', keepProducts)
                return res.status(messages.status.success).json({ message: `Successfully deleted product of id ${id}` })
            } else {
                return res.status(messages.status.unauthorized).json({ message: 'Only Admin can delete other\'s profile' })
            }
        }
    })
}

controllers.usersList = (req, res) => {
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.status(messages.status.forbidden).json({ err })
        } else {
            if (user[user.findIndex(e => e.email === authData.email)].userType === 'Admin') {
                return res.status(messages.status.success).json({ user })
            } else {
                return res.status(messages.status.unauthorized).json({ message: 'Only Admin can delete other\'s profile' })
            }
        }
    })
}

controllers.productsList = (req, res) => {
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.status(messages.status.forbidden).json({ err })
        } else {
            if (user[user.findIndex(e => e.email === authData.email)].userType === 'Admin') {
                return res.status(messages.status.success).json({ products })
            } else {
                return res.status(messages.status.unauthorized).json({ message: 'Only Admin can delete other\'s profile' })
            }
        }
    })
}

module.exports = controllers
