const validators = {}
const messages = require('../../messages/index')

validators.editProfile = (req, res, next) => {
    const { email, firstName, lastName, userType  } = req.body
    if (!email) return res.status(messages.status.forbidden).json(messages.messages.emailReqMsg)
    if (!firstName) return res.status(messages.status.forbidden).json(messages.messages.firstNameReqMsg)
    if (!lastName) return res.status(messages.status.forbidden).json(messages.messages.lastNameReqMsg)
    if (!userType) return res.status(messages.status.forbidden).json(messages.messages.userTypeReqMsg)
    next()
}
validators.deleteProfile = (req, res, next) => {
    const { email } = req.body
    if (!email) return res.status(messages.status.forbidden).json(messages.messages.emailReqMsg)
    next()
}
validators.editProduct = (req, res, next) => {
    const { id, productName, quantity, pricePP } = req.body
    if (!id) return res.status(messages.status.forbidden).json(messages.messages.idReqMsg)
    if (!productName) return res.status(messages.status.forbidden).json(messages.messages.productNameReqMsg)
    if (!quantity) return res.status(messages.status.forbidden).json(messages.messages.productQuantityReqMsg)
    if (!pricePP) return res.status(messages.status.forbidden).json(messages.messages.productPriceReqMsg)
    next()
}
validators.deleteProduct = (req, res, next) => {
    const { id } = req.body
    if (!id) return res.status(messages.status.forbidden).json(messages.messages.productPriceReqMsg)
    next()
}

module.exports = validators
