const app = require('express').Router()
const controllers = require('./controllers')
const validators = require('./validators')
const middleware = require('../user/middleware')

app.post('/edit-profile', validators.editProfile, middleware.verifyToken, controllers.editProfile)
app.post('/delete-profile', validators.deleteProfile, middleware.verifyToken, controllers.deleteProfile)
app.post('/edit-product', validators.editProduct, middleware.verifyToken, controllers.editProduct)
app.post('/delete-product', validators.deleteProduct, middleware.verifyToken, controllers.deleteProduct)
app.get('/users-list', middleware.verifyToken, controllers.usersList)
app.get('/products-list', middleware.verifyToken, controllers.productsList)

module.exports = app