const messages  = require("../../messages")

const validators = {}

validators.makeOrder = (req, res, next) => {
    const { productName, quantity } = req.body
    if(!productName) return res.status(messages.status.forbidden).json(messages.messages.productNameReqMsg)
    if(!quantity) return res.status(messages.status.forbidden).json(messages.messages.productQuantityReqMsg)
    if(quantity <= 0) return res.status(messages.status.forbidden).json(messages.messages.productQuantityReqMsg)
    next()
}

validators.sellProduct = (req, res, next) => {
    const { productName, quantity, pricePP } = req.body
    if(!productName) return res.status(messages.status.forbidden).json(messages.messages.productNameReqMsg)
    if(!quantity) return res.status(messages.status.forbidden).json(messages.messages.productQuantityReqMsg)
    if(quantity <= 0) return res.status(messages.status.forbidden).json(messages.messages.productQuantityReqMsg)
    if(!pricePP) return res.status(messages.status.forbidden).json(messages.messages.productPriceReqMsg)
    next()
}

module.exports = validators