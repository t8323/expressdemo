const app = require('express').Router()
const controllers = require('./controllers')
const validators = require('./validators')
const middleware = require('../user/middleware')

app.post('/make-order',validators.makeOrder, middleware.verifyToken,  controllers.makeOrder)
app.get('/my-orders', middleware.verifyToken, controllers.myOrders)
app.get('/show-products', middleware.verifyToken, controllers.showProducts)
app.post('/sell-product', validators.sellProduct, middleware.verifyToken,  controllers.sellProduct)

module.exports = app