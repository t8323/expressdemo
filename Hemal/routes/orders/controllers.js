const controllers = {}
const user = require('../user/data.json')
const products = require('./products.json')
const jwt = require('jsonwebtoken')
const messages = require('../../messages')
const utils = require('../../utils')
require('dotenv').config()
const secretKey = process.env.SECRET_KEY

controllers.makeOrder = (req, res) => {
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            res.status(messages.status.forbidden).json({ err })
        }
        else {
            const { productName, quantity } = req.body
            if (products.findIndex(e => e.productName === productName) < 0) return res.status(messages.status.forbidden).json(messages.messages.productUnavailableMsg)
            if (products[products.findIndex(e => e.productName === productName)].quantity < quantity) return res.status(messages.status.forbidden).json(messages.messages.productOutOfStockMsg)

            for (let i in products) {
                if (products[i].productName == productName) {
                    products[i].quantity = products[i].quantity - quantity
                }
            }
            utils.writeFile('./routes/orders/products.json', products)

            for (let i in user) {
                if (user[i].email == authData.email) {
                    user[i].myOrders.push(req.body)
                }
            }
            utils.writeFile('./routes/user/data.json', user)
            res.json(messages.messages.successfullOrderMsg)
        }
    })
}

controllers.myOrders = (req, res) => {
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            res.status(messages.status.forbidden).json({ err })
        } else {
            res.status(messages.status.success).json({ Orders: user[user.findIndex(e => e.email == authData.email)].myOrders })
        }
    })

}

controllers.showProducts = (req, res) => {
    jwt.verify(req.token, secretKey, (err, authData) => {
        if(err){
            res.status(messages.status.forbidden).json({ err })
        } else {
            if(user[user.findIndex(e=>e.email===authData.email)].userType === 'Seller'){
                const sellersProducts = products.filter(e=>e.email===authData.email)
                return res.status(messages.status.success).json({ sellersProducts })
            } else {
                return res.status(messages.status.success).json({ products })
            }
        }
    })
}

controllers.sellProduct = (req, res) => {
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            res.status(messages.status.forbidden).json({ err })
        } else {
            if(user[user.findIndex(e=>e.email===authData.email)].userType === 'Seller'){
                Object.assign(req.body, { id: new Date().getTime(), email: authData.email })
                products.push(req.body)
                utils.writeFile('./routes/orders/products.json', products)
                return res.status(messages.status.success).json({ message: 'Successfully added for selling' })
            } else {
                return res.status(messages.status.unauthorized).json({ message: 'Only seller can sell the product' })
            }
        }
    })
}

module.exports = controllers