const user = require('./data.json')
const products = require('../orders/products.json')
const messages = require('../../messages/index')
const controllers = {}
require('dotenv').config()
const jwt = require('jsonwebtoken')
const { encrypt, decrypt } = require('../../utils')
const utils = require('../../utils')
const secretKey = process.env.SECRET_KEY

controllers.register = (req, res) => {
    const { userType, email, password } = req.body
    if (user.findIndex((ele) => ele.email === email) > -1) return res.status(messages.status.not_acceptable).json(messages.messages.alreadyExistMsg)

    Object.assign(req.body, { password: encrypt(password), myOrders: [], token: [] })
    user.push(req.body)
    utils.writeFile('./routes/user/data.json', user)
    return res.status(messages.status.success).json(messages.messages.successfullRegMsg)
}

controllers.login = (req, res) => {
    const { email, password } = req.body

    if (user.findIndex((ele) => ele.email === email) < 0) return res.status(messages.status.unauthorized).json(messages.messages.notRegisteredMsg)
    if (user[user.findIndex((ele) => ele.email === email)].password !== encrypt(password)) return res.status(messages.status.unauthorized).json(messages.messages.invalidPasswordMsg)

    const token = jwt.sign({ email }, secretKey, { expiresIn: '24h' }, (err, token) => {
        const userToken = user[user.findIndex(e => e.email === email)].token
        if (userToken.findIndex(e => e === token) < 0) {
            if(user[user.findIndex(e => e.email === email)].token.length === 5) user[user.findIndex(e => e.email === email)].token.shift()
            user[user.findIndex(e => e.email === email)].token.push(token)
            utils.writeFile('./routes/user/data.json', user)
        }
        return res.json({ message: 'Login successfully!!', token });
    });
}

controllers.changePassword = (req, res) => {
    const { currentPass, newPass, confirmPass } = req.body
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.sendStatus(messages.status.forbidden)
        } else {
            if (user[user.findIndex(e => e.email === authData.email)].password !== encrypt(currentPass)) return res.json({ message: 'Enter correct current password' })
            if (newPass !== confirmPass) return res.json({ message: 'New password and confirm password must be same' })

            for (let key in user) {
                if (user[key].email == authData.email) {
                    user[key].password = encrypt(confirmPass)
                }
            }

            utils.writeFile('./routes/user/data.json', user)
            return res.status(messages.status.success).json(messages.messages.passwordChangedMsg)
        }
    })
}

controllers.editProfile = (req, res) => {
    const { firstName, lastName, email, userType } = req.body
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.sendStatus(messages.status.forbidden);
        } else {
            for (let key in user) {
                if (user[key].email == authData.email) {
                    user[key].firstName = firstName
                    user[key].lastName = lastName
                    user[key].email = email
                    user[key].userType = userType
                }
            }
            utils.writeFile('./routes/user/data.json', user)
            return res.json(messages.messages.profileUpadatedMsg)
        }
    });
}

controllers.deleteProfile = (req, res) => {
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.status(messages.status.forbidden).json({ err })
        } else {
            const keepProducts = products.filter(e => e.email !== authData.email)
            const keepProfiles = user.filter(e => e.email !== authData.email)

            if(keepProfiles.length === products.length) return res.json({ message: 'Profile not found' })

            utils.writeFile('./routes/orders/products.json', keepProducts)
            utils.writeFile('./routes/user/data.json', keepProfiles)
            return res.status(messages.status.success).json(messages.messages.profileDelSuccessMsg)
        }
    })
}

controllers.logout = (req, res) => {
    jwt.verify(req.token, secretKey, (err, authData) => {
        if (err) {
            return res.status(messages.status.forbidden).json({ err })
        } else {
            const userToken = user[user.findIndex(e => e.email === authData.email)].token
            const keepToken = userToken.filter(e => e !== req.token)
            user[user.findIndex(e => e.email === authData.email)].token = keepToken
            utils.writeFile('./routes/user/data.json', user)
            return res.status(messages.status.success).json(messages.messages.logoutSuccessMsg)
        }
    })
}

module.exports = controllers