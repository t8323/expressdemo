const app = require('express').Router()
const controllers = require('./controllers')
const validators = require('./validators')
const middleware = require('./middleware')

app.post('/register', validators.register, controllers.register)
app.post('/login', validators.login, controllers.login)
app.post('/change-password', middleware.verifyToken, validators.changePassword, controllers.changePassword)
app.post('/edit-profile', middleware.verifyToken, validators.editProfile, controllers.editProfile)
app.get('/delete-profile', middleware.verifyToken, controllers.deleteProfile)
app.get('/logout', middleware.verifyToken, controllers.logout)
module.exports = app
