const validators = {}
const validator = require('validator')
const messages = require('../../messages/index')

validators.register = (req, res, next) => {
    const { userType ,firstName, lastName, email, password } = req.body
    if (!firstName) return res.status(messages.status.forbidden).json(messages.messages.firstNameReqMsg)
    if (!lastName) return res.status(messages.status.forbidden).json(messages.messages.lastNameReqMsg)
    if (!userType) return res.status(messages.status.forbidden).json(messages.messages.userTypeReqMsg)
    if (!email && validator.isEmail(email)) return res.status(messages.status.forbidden).json(messages.messages.emailReqMsg)
    if (!password) return res.status(messages.status.forbidden).json(messages.messages.passwordReqMsg)
    next()
}
validators.login = (req, res, next) => {
    const { email, password } = req.body
    if (!email  && validator.isEmail(email)) return res.status(messages.status.forbidden).json(messages.messages.emailReqMsg)
    if (!password) return res.status(messages.status.forbidden).json(messages.messages.passwordReqMsg)
    next()
}
validators.changePassword = (req, res, next) => {
    const { currentPass, newPass, confirmPass } = req.body
    if(!currentPass) return res.status(messages.status.forbidden).json({ message:'Enter your current Password' })
    if(!newPass) return res.status(messages.status.forbidden).json({ message:'Enter your new Password' })
    if(!confirmPass) return res.status(messages.status.forbidden).json({ message:'Enter your new Password again' })
    next()
}
validators.editProfile = (req, res, next) => {
    const { firstName, lastName, email, userType } = req.body
    if (!firstName) return res.status(messages.status.forbidden).json(messages.messages.firstNameReqMsg)
    if (!lastName) return res.status(messages.status.forbidden).json(messages.messages.lastNameReqMsg)
    if (!userType) return res.status(messages.status.forbidden).json(messages.messages.userTypeReqMsg)
    if (!email && validator.isEmail(email)) return res.status(messages.status.forbidden).json(messages.messages.emailReqMsg)
    next()
}

module.exports = validators
