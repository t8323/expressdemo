const middleware = {}
const jwt = require('jsonwebtoken')
const utils = require('../../utils')
const user = require('./data.json')
const messages = require('../../messages/index')
require('dotenv').config()
const secretKey = process.env.SECRET_KEY

middleware.verifyToken = (req, res, next) => {

    const bearerHeader = req.headers['authorization'];
    
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];

        try {
            const authData = jwt.verify(bearerToken, secretKey)
            const userToken = user[user.findIndex(e=>e.email === authData.email)].token
            if (userToken.findIndex(e => e === bearerToken) < 0) return res.status(messages.status.unauthorized).json(messages.messages.needToLoginFirstMsg)
        } catch (error) {
            const authData = jwt.verify(bearerToken, secretKey, { ignoreExpiration: true })
            if(user.findIndex(e => e.email === authData) < 0) return res.status(messages.status.unauthorized).json(messages.messages.notRegisteredMsg)
            const userToken = user[user.findIndex(e=>e.email === authData.email)].token
            const keepTokens = userToken.filter(e=>e !== bearerToken)
            user[user.findIndex(e=>e.email===authData.email)].token = keepTokens
            utils.writeFile('./routes/user/data.json', user)
            return res.json({error: error.message, message: 'You need to login again'})
        }
        req.token = bearerToken;
        next();
    } else {
        res.status(messages.status.forbidden).json({ message: 'Authorization hearder is missing!' })
    }
}

module.exports = middleware