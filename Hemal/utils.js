const utils = {}
const { writeFileSync } = require('fs')
const path = require('path')

utils.encrypt = (password) => {
    return Buffer.from(password).toString('hex')
}

utils.decrypt = (encrypted) => {
    return Buffer.from(encrypted, 'hex').toString()
}

utils.writeFile = (filePath, data) => {
    writeFileSync(path.join(__dirname, filePath), JSON.stringify(data))
}

module.exports = utils