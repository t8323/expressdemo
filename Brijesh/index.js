require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const routes = require('./routes/route')

const app = express()

app.use(bodyParser.json())

app.use(cors())
app.use(helmet())
app.use('/api/v1', routes)


app.listen(3000, () => {
  console.log('listening on port 3000')
})
