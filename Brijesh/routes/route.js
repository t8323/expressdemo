const routes = require('express').Router()
const user = require('./users/auth')

routes.use('/user', user)

routes.all('*', (req, res) => {
  res.send('Route not found')
})

module.exports = routes
