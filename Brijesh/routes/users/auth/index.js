const router = require('express').Router()
const controllers = require('./lib/controllers')
const validators = require('./validators')

router.get('/get-users', controllers.getUsers)
router.post('/login', validators.login, controllers.login)
router.post('/signup', validators.signUp, controllers.signUp)

module.exports = router
