const user = require('../users.json')
const controllers = {}
const messages = require('../../../../messages')
const { writeFile, readFileSync, writeFileSync } = require('fs')

controllers.getUsers = (req, res) => {
  const users = user
  return res.json({ message: 'User fetch success', data: users })
}

controllers.login = async (req, res) => {
  try {
    const { sEmail, sPassword } = req.body
    if (user.findIndex((ele) => ele.email !== sEmail) < 0) return res.status(401).json({ message: 'email not registerd' })
    if (user[user.findIndex((ele) => ele.email === sEmail)].password !== sPassword) return res.status(401).json({ message: 'Password doesnt match' })
    user[user.findIndex((ele) => ele.email === sEmail)].isLoggedIn = true

    writeFileSync(require('path').join(__dirname, 'users.json'), JSON.stringify(user))

  } catch (error) {
    console.log(error)
    return res.status(500).json({ message: error })
  }
}

controllers.signUp = (req, res) => {
  const { fName, lName, email, password } = req.body
  if (user.findIndex((ele) => ele.email === email) > -1) return res.status(406).json({ message: 'emailId already exists' })
  Object.assign(req.body, { id: new Date().getTime() })
  user.push(req.body)
  writeFileSync(require('path').join(__dirname, 'users.json'), JSON.stringify(user))
  return res.status(messages.status.statusSuccess).json(messages.messages.registeredSuccess)
}

module.exports = controllers