const router = require('express').Router()
const controllers = require('./controllers')
const validators = require('./validators')

router.get('/get-users', controllers.getUsers)
router.post('/login', validators.login, controllers.login)
router.post('/signup', validators.signUp, controllers.signUp)
router.delete('/delete',controllers.delete)
router.post('/changepass',controllers.changePass)
router.post('/editprofile',controllers.ediProfile)

module.exports = router
