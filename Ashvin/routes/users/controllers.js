const user = require('./users.json')
const controllers = {}
const messages = require('../../messages')
const { writeFileSync } = require('fs')

controllers.getUsers = (req, res) => {
  const users = user
  return res.json({ message: 'User fetch success', data: users })
}
//signup
controllers.signUp = (req, res) => {
  const { fName, lName, email, password } = req.body
  if (user.findIndex((ele) => ele.email === email) > -1) 
    return res.status(406).json({ message: 'emailId already exists' })
    Object.assign( req.body, { id: new Date().getTime() })
    user.push(req.body)
    writeFileSync(require('path').join(__dirname, 'users.json'), JSON.stringify(user))
    // console.log(user[user.length-1].fName + " register to the database");
    return res.status(messages.status.statusSuccess).json(messages.messages.registeredSuccess)
}
//signin
controllers.login = (req, res) => {
  const { sEmail, sPassword } = req.body;
  const us = user.findIndex((ele) => ele.email === sEmail);
  if (us > -1 && user[us].password === sPassword) {
    user[us].isLoggedIn = true;
    user[user.findIndex((ele) => ele.orderArray)].orderArray = [ item, quantity];    
    writeFileSync(
      require("path").join(__dirname, "users.json"),JSON.stringify(user));
    return res.status(messages.status.statusSuccess).json(messages.messages.loginSuccess);
  }
  if (us > -1 && user[us].password !=sPassword) {
    return res.status(messages.status.badRequest).json(messages.messages.wrongpass);
  } else {
    return res.status(messages.status.statusNotFound).json(messages.messages.EmailNotfound);
  }
}
//delete
controllers.delete = (req,res)=>{
  const {eEmail, sPassword } = req.body;
  let del = user.findIndex((ele) => ele.email === eEmail)
  if(del > -1 && user[del].password === sPassword){
    user.splice(del,1);
    writeFileSync(require("path").join(__dirname, "users.json"),JSON.stringify(user));
    return res.json({ message: "User Deleted" });
  }else {
    res.json({ message: "User not found" });
  }
}


//change pass
controllers.changePass = (req, res) => {
  const { eEmail, oPassword, nPassword } = req.body;
  const passM = user.some((ele) => ele.email === eEmail && ele.password === oPassword);
  const passNotM = user.some((ele) => ele.email === eEmail && ele.password !== oPassword);
  if (passM) { 
    user.forEach((ele) => { if (ele.password === oPassword) ele.password = nPassword ? nPassword : ele.password});
    writeFileSync(require("path").join(__dirname, "users.json"),JSON.stringify(user));
    return res.status(messages.status.statusSuccess).json({message: "changed Password successfully"});
  }
  if(passNotM){
    return res.status(messages.status.statusNotFound).json({message: "Password doesnt match"});
  } else {
    return res.status(messages.status.statusNotFound).json({message: "Email is require"});
  }
}
//edit profile
controllers.ediProfile = (req, res) => {
  const { fName, lName, email } = req.body;
  const find = user.some((ele) => ele.email === email);
  if (find) {
    user.forEach((ele) => {
      if (ele.email === email) {
        ele.fName = fName ? fName : ele.fName;
        ele.lName = lName ? lName : ele.lName;
      }});
    writeFileSync(require("path").join(__dirname, "users.json"),JSON.stringify(user));
    return res.status(messages.status.statusSuccess).json({message:"Profile successfully updated"});
  } else {
    return res.status(messages.status.statusNotFound).json({ messaege: "Email not registered" });
  }
}

module.exports = controllers