const validators = {}

validators.login = (req, res, next) => {
  const { sEmail, sPassword } = req.body
  if (!sEmail) return res.status(403).json({ message: 'Email is requierd' })
  if (!sPassword) return res.status(403).json({ message: 'Password is requierd' })
  next()
}

validators.signUp = (req, res, next) => {
  const { fName, lName, email, password } = req.body
  if (!email) return res.status(403).json({ message: 'Email is requierd' })
  if (!password) return res.status(403).json({ message: 'Password is requierd' })
  if (!fName) return res.status(403).json({ message: 'Firstname is requierd' })
  if (!lName) return res.status(403).json({ message: 'Lastname is requierd' })
  next()
}

module.exports = validators
