const routes = require('express').Router()
const user = require('./users')
const order = require('./orders')

routes.use('/user', user)
routes.use('/order', order)

routes.all('*', (req, res) => {
  res.send('Route not found')
})

module.exports = routes
