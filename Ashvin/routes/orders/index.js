const router = require('express').Router()
const controllers = require('./controller')

router.get('/get_stock', controllers.getStock)
router.post('/sell',  controllers.sellStock)
router.post('/getorder',  controllers.getOrder)


module.exports = router