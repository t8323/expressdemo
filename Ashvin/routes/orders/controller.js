const order = require("./order.json");
const stock = require("./stock.json");
const controllers = {};
const { writeFile, writeFileSync } = require("fs");

controllers.getStock = (req, res) => {
  res.json({ data: stock });
};

controllers.sellStock = (req, res) => {
  const { email, item } = req.body;
  if (stock.some((ele) => req.body.item == ele.item && ele.quantity > 0)) {
    const st = { email, item, id: Date.now() };
    order.push(st);
    writeFile(
      require("path").join(__dirname, "order.json"),
      JSON.stringify(order),
      (err) => {
        if (err) return res.json({ message: "Error occur" });
      }
    );
    const upStock = stock.map((ele) =>
      ele.item == item ? { ...ele, quantity: parseInt(ele.quantity - 1) } : ele
    );
    writeFileSync(
      require("path").join(__dirname, "stock.json"),
      JSON.stringify(upStock)
    );
    return res.json({ message: "order successfuly palced" });
  } else {
    return res.json({ message: "out of stock" });
  }
};
controllers.getOrder = (req, res) => {
  const { email } = req.body;
  const userorder = order.filter((ele) => ele.email == email);
  res.json(userorder);
};
module.exports = controllers;
