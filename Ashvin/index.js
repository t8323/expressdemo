require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const routes = require('./routes/route')

const app = express()

app.use(bodyParser.json())

app.use(cors())
app.use(helmet())
app.use('/api/v1', routes)

const port = process.env.PORT ||8080;
app.listen(port, () => {
  console.log("Listening " + process.env.PORT);
});