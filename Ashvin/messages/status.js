const status = {
  statusSuccess: 200,
  statusNotFound: 403,
  badRequest: 400,

}

module.exports = status